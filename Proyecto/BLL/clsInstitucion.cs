﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    class clsInstitucion
    {
		public List<ConsultarInstitucionResult> ConsultarInstitucion()
		{
			try
			{
				DatosDataContext db = new DatosDataContext();
				List<ConsultarInstitucionResult> datos = db.ConsultarInstitucion().ToList();
				return datos;
			}
			catch (Exception ex)
			{

				throw;
			}

		}
		public ConsultaInstitucionResult ConsultaInstitucion(int IdInstitucion)
		{
			try
			{
				DatosDataContext db = new DatosDataContext();
				ConsultaInstitucionResult dato = db.ConsultaInstitucion(IdInstitucion).FirstOrDefault();
				return dato;
			}
			catch (Exception ex)
			{

				throw;
			}
		}
		public bool ActualizarInstitucion(int IdInstitucion, string Descripcion, string Telefono, string Encaegado,
			string Direccion, char Provincia, string Canton, string Distrito, bool Estado)
		{
			try
			{
				DatosDataContext db = new DatosDataContext();
				db.ActualizarInstitucion(IdInstitucion, Descripcion, Telefono, Encaegado, Direccion, Provincia,
					Canton, Distrito, Estado);
				return true;
			}
			catch (Exception ex)
			{
				return false;
				throw;
			}
		}
		public bool AgregarInstitucion(string Descripcion, string Telefono, string Encaegado,
			string Direccion, char Provincia, string Canton, string Distrito, bool Estado)
		{
			try
			{
				DatosDataContext db = new DatosDataContext();
				db.AgregarInstitucion(Descripcion, Telefono, Encaegado, Direccion, Provincia,
					Canton, Distrito, Estado);
				return true;
			}
			catch (Exception ex)
			{
				return false;
				throw;
			}
		}
		public bool EliminarInstitucion(int IdInstitucion)
		{
			try
			{
				DatosDataContext db = new DatosDataContext();
				db.EliminarInstitucion(IdInstitucion);
				return true;
			}
			catch (Exception ex)
			{
				return false;
				throw;
			}
		}
	}
}
