﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    class clsNota
    {
		public List<ConsultarNotaResult> ConsultarNota()
		{
			try
			{
				DatosDataContext db = new DatosDataContext();
				List<ConsultarNotaResult> datos = db.ConsultarNota().ToList();
				return datos;
			}
			catch (Exception ex)
			{

				throw;
			}

		}
		public ConsultaNotaResult ConsultaNota(int IdNota)
		{
			try
			{
				DatosDataContext db = new DatosDataContext();
				ConsultaNotaResult dato = db.ConsultaNota(IdNota).FirstOrDefault();
				return dato;
			}
			catch (Exception ex)
			{

				throw;
			}
		}
		public bool ActualizarNota(int IdNota, int IdEstudiante, int IdInstitucion, decimal Nota, string Estado)
		{
			try
			{
				DatosDataContext db = new DatosDataContext();
				db.ActualizarNota(IdNota, IdEstudiante, IdInstitucion, Nota, Estado);
				return true;
			}
			catch (Exception ex)
			{
				return false;
				throw;
			}
		}
		public bool AgregarNota(string IdEstudiante, int IdInstitucion, decimal Nota, string Estado)
		{
			try
			{
				DatosDataContext db = new DatosDataContext();
				db.AgregarNota(IdEstudiante, IdInstitucion, Nota, Estado);
				return true;
			}
			catch (Exception ex)
			{
				return false;
				throw;
			}
		}
		public bool EliminarNota(int IdNota)
		{
			try
			{
				DatosDataContext db = new DatosDataContext();
				db.EliminarNota(IdNota);
				return true;
			}
			catch (Exception ex)
			{
				return false;
				throw;
			}
		}
	}
}
