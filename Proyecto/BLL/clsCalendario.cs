﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    class clsCalendario
    {
		public List<ConsultarCalendarioResult> ConsultarCalendario()
		{
			try
			{
				DatosDataContext db = new DatosDataContext();
				List<ConsultarCalendarioResult> datos = db.ConsultarCalendario().ToList();
				return datos;
			}
			catch (Exception ex)
			{

				throw;
			}
		}
		public ConsultaCalendarioResult ConsultaCalendario(int IdEvento)
		{
			try
			{
				DatosDataContext db = new DatosDataContext();
				ConsultaCalendarioResult dato = db.ConsultaCalendario(IdEvento).FirstOrDefault();
				return dato;
			}
			catch (Exception ex)
			{

				throw;
			}
		}
		public bool ActualizarCalendario(int IdEvento, string Asunto, string Descripcion, DateTime Inicio, DateTime Finaliza, bool TodoElDia, string Color)
		{
			try
			{
				DatosDataContext db = new DatosDataContext();
				db.ActualizarCalendario(IdEvento, Asunto, Descripcion, Inicio, Finaliza, TodoElDia, Color);
				return true;
			}
			catch (Exception ex)
			{
				return false;
				throw;
			}
		}
		public bool AgregarCalendario(string Asunto, string Descripcion, DateTime Inicio, DateTime Finaliza, bool TodoElDia, string Color)
		{
			try
			{
				DatosDataContext db = new DatosDataContext();
				db.AgregarCalendario(Asunto, Descripcion, Inicio, Finaliza, TodoElDia, Color);
				return true;
			}
			catch (Exception ex)
			{
				return false;
				throw;
			}
		}
		public bool EliminarCalendario(int IdEvento)
		{
			try
			{
				DatosDataContext db = new DatosDataContext();
				db.EliminarCalendario(IdEvento);
				return true;
			}
			catch (Exception ex)
			{
				return false;
				throw;
			}
		}
	}
}
