﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    class clsEstudiante
    {
		public List<ConsultarEstudianteResult> ConsultarEstudiante()
		{
			try
			{
				DatosDataContext db = new DatosDataContext();
				List<ConsultarEstudianteResult> datos = db.ConsultarEstudiante().ToList();
				return datos;
			}
			catch (Exception ex)
			{

				throw;
			}

		}
		public ConsultaEstudianteResult ConsultaEstudiante(int IdEstudiante)
		{
			try
			{
				DatosDataContext db = new DatosDataContext();
				ConsultaEstudianteResult dato = db.ConsultaEstudiante(IdEstudiante).FirstOrDefault();
				return dato;
			}
			catch (Exception ex)
			{

				throw;
			}
		}
		public bool ActualizarEstudiante(int IdEstudiante, int IdInstitucion, int IdUsuario,
			int IdGrado, bool Estado)
		{
			try
			{
				DatosDataContext db = new DatosDataContext();
				db.ActualizarEstudiante(IdEstudiante, IdInstitucion,
					IdUsuario, IdGrado, Estado);
				return true;
			}
			catch (Exception ex)
			{
				return false;
				throw;
			}
		}
		public bool AgregarEstudiante(int IdInstitucion, int IdUsuario,
			int IdGrado, bool Estado)
		{
			try
			{
				DatosDataContext db = new DatosDataContext();
				db.AgregarEstudiante(IdInstitucion,
					IdUsuario, IdGrado, Estado);
				return true;
			}
			catch (Exception ex)
			{
				return false;
				throw;
			}
		}
		public bool EliminarEstudiante(int IdEstudiante)
		{
			try
			{
				DatosDataContext db = new DatosDataContext();
				db.EliminarEstudiante(IdEstudiante);
				return true;
			}
			catch (Exception ex)
			{
				return false;
				throw;
			}
		}
	}
}
