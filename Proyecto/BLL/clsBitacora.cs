﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class clsBitacora
    {
		public List<ConsultarBitacoraResult> ConsultarBitacora()
		{
			try
			{
				DatosDataContext db = new DatosDataContext();
				List<ConsultarBitacoraResult> datos = db.ConsultarBitacora().ToList();
				return datos;
			}
			catch (Exception ex)
			{

				throw;
			}
		}
		public ConsultaBitacoraResult ConsultaBitacora(int IdBitacora)
		{
			try
			{
				DatosDataContext db = new DatosDataContext();
				ConsultaBitacoraResult dato = db.ConsultaBitacora(IdBitacora).FirstOrDefault();
				return dato;
			}
			catch (Exception ex)
			{

				throw;
			}
		}
		public bool ActualizarBitacora(int IdBitacora, string Controlador, string Accion, string Error, int Tipo, DateTime Fecha, int IdUsuario)
		{
			try
			{
				DatosDataContext db = new DatosDataContext();
				db.ActualizarBitacora(IdBitacora, Controlador, Accion, Error, Tipo, Fecha, IdUsuario);
				return true;
			}
			catch (Exception ex)
			{
				return false;
				throw;
			}
		}
		public bool AgregarBitacora(string Controlador, string Accion, string Error, int Tipo, DateTime Fecha, int IdUsuario)
		{
			try
			{
				DatosDataContext db = new DatosDataContext();
				db.AgregarBitacora(Controlador, Accion, Error, Tipo, Fecha, IdUsuario);
				return true;
			}
			catch (Exception ex)
			{
				return false;
				throw;
			}
		}
	}
}
