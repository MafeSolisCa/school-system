﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    class clsMateria
    {
		public List<ConsultarMateriaResult> ConsultarMateria()
		{
			try
			{
				DatosDataContext db = new DatosDataContext();
				List<ConsultarMateriaResult> datos = db.ConsultarMateria().ToList();
				return datos;
			}
			catch (Exception ex)
			{

				throw;
			}

		}
		public ConsultaMateriaResult ConsultaMateria(int IdMateria)
		{
			try
			{
				DatosDataContext db = new DatosDataContext();
				ConsultaMateriaResult dato = db.ConsultaMateria(IdMateria).FirstOrDefault();
				return dato;
			}
			catch (Exception ex)
			{

				throw;
			}
		}
		public bool ActualizarMateria(int IdMateria, string Descripcion, int IdInstitucion, bool Estado)
		{
			try
			{
				DatosDataContext db = new DatosDataContext();
				db.ActualizarMateria(IdMateria, Descripcion, IdInstitucion, Estado);
				return true;
			}
			catch (Exception ex)
			{
				return false;
				throw;
			}
		}
		public bool AgregarMateria(string Descripcion, int IdInstitucion, bool Estado)
		{
			try
			{
				DatosDataContext db = new DatosDataContext();
				db.AgregarMateria(Descripcion, IdInstitucion, Estado);
				return true;
			}
			catch (Exception ex)
			{
				return false;
				throw;
			}
		}
		public bool EliminarMateria(int IdMateria)
		{
			try
			{
				DatosDataContext db = new DatosDataContext();
				db.EliminarMateria(IdMateria);
				return true;
			}
			catch (Exception ex)
			{
				return false;
				throw;
			}
		}
	}
}
