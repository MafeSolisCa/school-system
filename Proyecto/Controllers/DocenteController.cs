using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAL;
using BLL;
using Proyecto.Models;

namespace Proyecto.Controllers
{
    [Authorize]
    
  public class DocenteController : Controller
    {
        clsInstitucion Institucion = new clsInstitucion();
        clsUsuario Usuario = new clsUsuario();
        clsMateria Materia = new clsMateria();
        
        /// <summary>
        /// Metodo GET del Index de Docente
        /// </summary>
        /// <returns></returns>
        //[AuthorizeRole(Role.Administrador)]
       // [HttpPost]
        public ActionResult Index()
        {
            try
            {
                List<DocenteList> listaDocente = new List<DocenteList>();
                clsDocente Docente = new clsDocente();
                var data = Docente.ConsultarDocenteLista().ToList();

                foreach (var item in data)
                {
                    DocenteList modelo = new DocenteList();
                    modelo.IdDocente = item.idDocente;
                    modelo.InstitucionNombre = item.InstitucionNombre;              
                    modelo.Nombre = item.Nombre;
                    modelo.MateriaNombre = item.MateriaNombre;
                    modelo.Estado = item.Estado;
                    listaDocente.Add(modelo);
                }
                return View(listaDocente);
            }
            catch
            {
        string descMsg = "Error consultando la informacion de Docente";
        int us = Convert.ToInt32(Session["IdUsuario"].ToString());
        Objbitacora.AgregarBitacora("DocenteController", "Index", descMsg, 1, DateTime.UtcNow.Date, us);

        return RedirectToAction("Error", "Error");
            }          

        }
        public ActionResult Crear()
        {
            try
            {
                ViewBag.ListaInstitucion = CargarInstitucion();
                ViewBag.ListaUsuario = CargarUsuario();
                ViewBag.ListaMateria = CargarMateria();
                ViewBag.ListaEstados = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");
                return View();
            }
            catch (Exception)
            {

                throw;
            }

        }

    clsBitacora Objbitacora = new clsBitacora();
    // POST: Docente/Crear
    [HttpPost]
        public ActionResult Crear(Docente docente)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    clsDocente ObjDocente = new clsDocente();
                    bool Resultado = ObjDocente.AgregarDocente(docente.IdInstitucion, docente.IdUsuario, docente.IdMateria, 
                      docente.Estado);
                    clsBitacora Objbitacora = new clsBitacora();

                    if (Resultado)
                    {
            int us = Convert.ToInt32(Session["IdUsuario"].ToString());
            Objbitacora.AgregarBitacora("DocenteController", "Crear", "Exito", 1, DateTime.UtcNow.Date, us);

            return RedirectToAction("Index");
                    }
                    else
                    {
                        ViewBag.ListaInstitucion = CargarInstitucion();
                        ViewBag.ListaUsuario = CargarUsuario();
                        ViewBag.ListaMateria = CargarMateria();
                        ViewBag.ListaEstados = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");
            int us = Convert.ToInt32(Session["IdUsuario"].ToString());
            Objbitacora.AgregarBitacora("DocenteController", "Crear", "Error datos", 1, DateTime.UtcNow.Date, us);

            return View("Crear");
                    }
                }
                else
                {
                    ViewBag.ListaInstitucion = CargarInstitucion();
                    ViewBag.ListaUsuario = CargarUsuario();
                    ViewBag.ListaMateria = CargarMateria();
                    ViewBag.ListaEstados = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");
          int us = Convert.ToInt32(Session["IdUsuario"].ToString());
          Objbitacora.AgregarBitacora("DocenteController", "Crear", "Error datos", 1, DateTime.UtcNow.Date, us);

          return View("Crear");
                }

            }
            catch
            {
                return View();
            }
        }

        public ActionResult Editar(int id)
        {
            try
            {
                clsDocente ObjDocente = new clsDocente();
        ViewBag.ListaInstitucion = CargarInstitucion();
        ViewBag.ListaUsuario = CargarUsuario();
        ViewBag.ListaMateria = CargarMateria();
        ViewBag.ListaEstados = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");


        var dato = ObjDocente.ConsultaDocente(id);

                Docente modelo = new Docente();
                modelo.IdDocente = dato.IdDocente;               
                modelo.IdInstitucion = dato.IdInstitucion;
                modelo.IdUsuario = dato.IdUsuario;
                modelo.IdMateria = Convert.ToInt32(dato.IdMateria);
                modelo.Estado = dato.Estado;
                return View(modelo);
            }
            catch (Exception)
            {

                throw;
            }

        }
        // POST: Docente/Editar/5
        [HttpPost]
        public ActionResult Editar(int id, Docente docente)
        {
            try
            {

                clsDocente ObjDocente = new clsDocente();
                bool Resultado = ObjDocente.ActualizarDocente(id, docente.IdInstitucion, docente.IdUsuario, docente.IdMateria, docente.Estado);

                if (Resultado)
                {
          int us = Convert.ToInt32(Session["IdUsuario"].ToString());
          Objbitacora.AgregarBitacora("DocenteController", "Editar", "Exito", 1, DateTime.UtcNow.Date, us);

          return RedirectToAction("Index");
                }
                else
                {

                    ViewBag.ListaInstitucion = CargarInstitucion();
                    ViewBag.ListaUsuario = CargarUsuario();
                    ViewBag.ListaMateria = CargarMateria();
                    ViewBag.ListaEstados = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");
                    return View("Editar");
                }
            }
            catch
            {
                return View();
            }
        }
            // POST: Docente/Eliminar/5
            public ActionResult Eliminar(int id)
            {
                try
                {
                clsDocente ObjDocente = new clsDocente();
                bool Resultado = ObjDocente.EliminarDocente(id);

                    if (Resultado)
                    {
          int us = Convert.ToInt32(Session["IdUsuario"].ToString());
          Objbitacora.AgregarBitacora("DocenteController", "Eliminar", "Exito", 1, DateTime.UtcNow.Date, us);

          return RedirectToAction("Index");
                    }
                    else
                    {
                        return RedirectToAction("Index");
                    }

                }
                catch
                {
        int us = Convert.ToInt32(Session["IdUsuario"].ToString());
        Objbitacora.AgregarBitacora("DocenteController", "Eliminar", "Error en la eliminacion", 1, DateTime.UtcNow.Date, us);

        return View();
                }
            }
        
        
        public List<ConsultarInstitucionResult> CargarInstitucion()
        {
            List<ConsultarInstitucionResult> instituciones = Institucion.ConsultarInstitucion();
            return instituciones;
        }

        public List<ConsultarUsuarioViewBagResult> CargarUsuario()
        {
            List<ConsultarUsuarioViewBagResult> usuario = Usuario.ConsultarUsuarioViewBag();
            return usuario;
        }

        public List<ConsultarMateriaResult> CargarMateria()
        {
            List<ConsultarMateriaResult> materia = Materia.ConsultarMateria();
            return materia;
        }
    }
}
