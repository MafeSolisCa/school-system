using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAL;
using BLL;
using Proyecto.Models;

namespace Proyecto.Controllers
{
    [Authorize]
    public class UsuarioRolController : Controller
    {
    clsBitacora Objbitacora = new clsBitacora();
    [Authorize(Roles = "Administrador")]
    public ActionResult Index()
        {
            try
            {
                List<UsuarioRolList> listaRol = new List<UsuarioRolList>();
                clsUsuarioRol UsuarioRol = new clsUsuarioRol();
                var data = UsuarioRol.ConsultarUsuarioRolLista().ToList();

                foreach (var item in data)
                {
                    UsuarioRolList modelo = new UsuarioRolList();
                    modelo.Id = item.Id;
                    modelo.Nombre = item.Nombre;
                    modelo.Rol = item.Rol;
                    listaRol.Add(modelo);
                }
                return View(listaRol);
            }
            catch
            {
        int us = Convert.ToInt32(Session["IdUsuario"].ToString());
        Objbitacora.AgregarBitacora("UsuarioRolController", "Index", "Eror en la consulta de los datos", 1, DateTime.UtcNow.Date, us);

        return RedirectToAction("Error", "Error");
            }
        }
    [Authorize(Roles = "Administrador")]
    public ActionResult Crear()
        {
            try
            {

                ViewBag.ListaUsuario = CargarUsuario();
                ViewBag.ListaRol = CargarRol();

                return View();
            }
            catch (Exception)
            {

                throw;
            }

        }


    [Authorize(Roles = "Administrador")]
    [HttpPost]
        public ActionResult Crear(UsuarioRol usuarioRol)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    clsUsuarioRol UsuarioRol = new clsUsuarioRol();
                    bool Resultado = UsuarioRol.AgregarUsuarioRol(usuarioRol.IdUsuario, usuarioRol.IdRol);
                    clsBitacora Objbitacora = new clsBitacora();

                    if (Resultado)
                    {
            int us = Convert.ToInt32(Session["IdUsuario"].ToString());
            Objbitacora.AgregarBitacora("UsuarioRolController", "Crear", "Exito", 1, DateTime.UtcNow.Date, us);

            return RedirectToAction("Index");
                    }
                    else
                    {
                        ViewBag.ListaUsuario = CargarUsuario();
                        ViewBag.ListaRol = CargarRol();

                        return View("Crear");
                    }
                }
                else
                {
                    ViewBag.ListaUsuario = CargarUsuario();
                    ViewBag.ListaRol = CargarRol();

                    return View("Crear");
                }

            }
            catch
            {
        int us = Convert.ToInt32(Session["IdUsuario"].ToString());
        Objbitacora.AgregarBitacora("UsuarioRolController", "Crear", "Error en creacion de los datos", 1, DateTime.UtcNow.Date, us);

        return View();
            }
        }
    [Authorize(Roles = "Administrador")]
    public ActionResult Editar(int id)
        {
            try
            {
        ViewBag.ListaUsuario = CargarUsuario();
        ViewBag.ListaRol = CargarRol();

        clsUsuarioRol UsuarioRol = new clsUsuarioRol();

                var dato = UsuarioRol.ConsultaUsuarioRol(id);
                UsuarioRol modelo = new UsuarioRol();
                modelo.IdUsuarioRol = dato.Id;
                modelo.IdUsuario = dato.IdUsuario;
                modelo.IdRol = dato.IdRol;
                return View(modelo);
            }
            catch (Exception)
            {

                throw;
            }

        }
    [Authorize(Roles = "Administrador")]
    [HttpPost]
        public ActionResult Editar(int id, UsuarioRol usuarioRol)
        {
            try
            {
                clsUsuarioRol UsuarioRol = new clsUsuarioRol();
                bool Resultado = UsuarioRol.ActualizarUsuarioRol(id, usuarioRol.IdUsuario, usuarioRol.IdRol);

                if (Resultado)
                {
          int us = Convert.ToInt32(Session["IdUsuario"].ToString());
          Objbitacora.AgregarBitacora("UsuarioRolController", "Editar", "Exito", 1, DateTime.UtcNow.Date, us);

          return RedirectToAction("Index");
                }
                else
                {

                    ViewBag.ListaUsuario = CargarUsuario();
                    ViewBag.ListaRol = CargarRol();
                    return View("Editar");
                }
            }
            catch
            {
        int us = Convert.ToInt32(Session["IdUsuario"].ToString());
        Objbitacora.AgregarBitacora("UsuarioRolController", "Editar", "Error en la edicion de los datos", 1, DateTime.UtcNow.Date, us);

        return View();
            }
        }
    [Authorize(Roles = "Administrador")]
    public ActionResult Eliminar(int id)
        {
            try
            {
                clsUsuarioRol UsuarioRol = new clsUsuarioRol();
                bool Resultado = UsuarioRol.EliminarUsuarioRol(id);

                if (Resultado)
                {
          int us = Convert.ToInt32(Session["IdUsuario"].ToString());
          Objbitacora.AgregarBitacora("UsuarioRolController", "Eliminar", "Exito", 1, DateTime.UtcNow.Date, us);

          return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Index");
                }

            }
            catch
            {
        int us = Convert.ToInt32(Session["IdUsuario"].ToString());
        Objbitacora.AgregarBitacora("UsuarioRolController", "Eliminar", "Error en la eliminacion de los datos", 1, DateTime.UtcNow.Date, us);

        return View();
            }
        }
        clsUsuario Usuario = new clsUsuario();
        public List<ConsultarUsuarioViewBagResult> CargarUsuario()
        {
            List<ConsultarUsuarioViewBagResult> usuario = Usuario.ConsultarUsuarioViewBag();
            return usuario;
        }
        clsRol Rol = new clsRol();
        public List<ConsultarRolResult> CargarRol()
        {
            List<ConsultarRolResult> rol = Rol.ConsultarRol();
            return rol;
        }
    }
}
