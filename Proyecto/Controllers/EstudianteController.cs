using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAL;
using BLL;
using Proyecto.Models;

namespace Proyecto.Controllers
{
    [Authorize]
    public class EstudianteController : Controller
    {
    clsDocente ObjDocente = new clsDocente();
    clsBitacora Objbitacora = new clsBitacora();
    [Authorize(Roles = "Administrador")]
    public ActionResult Index()
        {
            try
            {
                List<EstudianteList> listaEstudiante = new List<EstudianteList>();
                clsEstudiante Estuadiante = new clsEstudiante();
                var data = Estuadiante.ConsultarEstudianteLista().ToList();

                foreach (var item in data)
                {
                    EstudianteList modelo = new EstudianteList();
                    modelo.IdEstudiante = item.IdEstudiante;
                    modelo.Institucion = item.Institucion;
                    modelo.Cedula = item.Cedula;
                    modelo.Nombre = item.Nombre;
                    modelo.Grado = item.Grado;
                    modelo.Estado = item.Estado;
                    listaEstudiante.Add(modelo);
                }
                return View(listaEstudiante);
            }
            catch
            {
        int us = Convert.ToInt32(Session["IdUsuario"].ToString());
        Objbitacora.AgregarBitacora("EstudianteController", "Index", "Error en la consulta de los datos", 1, DateTime.UtcNow.Date, us);

        return RedirectToAction("Error", "Error");
            }
        }
    [Authorize(Roles = "Administrador")]
    public ActionResult Crear()
        {
            try
            {
                ViewBag.ListaInstitucion = CargarInstitucion();
                ViewBag.ListaUsuario = CargarUsuario();
                ViewBag.ListaGrado = CargarGrado();
                ViewBag.ListaEstados = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");
                return View();
            }
            catch (Exception)
            {

                throw;
            }

        }
    [Authorize(Roles = "Administrador")]
    [HttpPost]
        public ActionResult Crear(Estudiante estudiante)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    clsEstudiante ObjEstudiante = new clsEstudiante();
                    bool Resultado = ObjEstudiante.AgregarEstudiante(estudiante.IdInstitucion, estudiante.IdUsuario, estudiante.IdGrado, estudiante.Estado);

                    if (Resultado)
                    {
            int us = Convert.ToInt32(Session["IdUsuario"].ToString());
            Objbitacora.AgregarBitacora("EstudianteController", "Crear", "Exito", 1, DateTime.UtcNow.Date, us);

            return RedirectToAction("Index");
                    }
                    else
                    {
                        ViewBag.ListaInstitucion = CargarInstitucion();
                        ViewBag.ListaUsuario = CargarUsuario();
                        ViewBag.ListaGrado = CargarGrado();
                        ViewBag.ListaEstados = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");

                        return View("Crear");
                    }
                }
                else
                {
                    ViewBag.ListaInstitucion = CargarInstitucion();
                    ViewBag.ListaUsuario = CargarUsuario();
                    ViewBag.ListaGrado = CargarGrado();
                    ViewBag.ListaEstados = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");

                    return View("Crear");
                }

            }
            catch
            {
        int us = Convert.ToInt32(Session["IdUsuario"].ToString());
        Objbitacora.AgregarBitacora("EstudianteController", "Crear", "Error en la creacion de los datos", 1, DateTime.UtcNow.Date, us);

        return View();
            }
        }
    [Authorize(Roles = "Administrador")]
    public ActionResult Editar(int id)
        {
            try
            {
                clsEstudiante ObjEstudiante = new clsEstudiante();
        ViewBag.ListaInstitucion = CargarInstitucion();
        ViewBag.ListaUsuario = CargarUsuario();
        ViewBag.ListaGrado = CargarGrado();
        ViewBag.ListaEstados = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");
        var dato = ObjEstudiante.ConsultaEstudiante(id);

                Estudiante modelo = new Estudiante();
                modelo.IdEstudiante = dato.IdEstudiante;
                modelo.IdInstitucion = dato.IdInstitucion;
                modelo.IdUsuario = dato.IdUsuario;
                modelo.IdGrado = Convert.ToInt32(dato.IdGrado);
                modelo.Estado = dato.Estado;
                return View(modelo);
            }
            catch (Exception)
            {

                throw;
            }

        }
    [Authorize(Roles = "Administrador")]
    [HttpPost]
        public ActionResult Editar(int id, Estudiante estudiante)
        {
            try
            {

                clsEstudiante ObjEstudiante = new clsEstudiante();
                bool Resultado = ObjEstudiante.ActualizarEstudiante(id, estudiante.IdInstitucion, estudiante.IdUsuario, estudiante.IdGrado, estudiante.Estado);

                if (Resultado)
                {
          int us = Convert.ToInt32(Session["IdUsuario"].ToString());
          Objbitacora.AgregarBitacora("EstudianteController", "Editar", "Exito", 1, DateTime.UtcNow.Date, us);

          return RedirectToAction("Index");
                }
                else
                {

                    ViewBag.ListaInstitucion = CargarInstitucion();
                    ViewBag.ListaUsuario = CargarUsuario();
                    ViewBag.ListaGrado = CargarGrado();
                    ViewBag.ListaEstados = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");
                    return View("Editar");
                }
            }
            catch
            {
        int us = Convert.ToInt32(Session["IdUsuario"].ToString());
        Objbitacora.AgregarBitacora("EstudianteController", "Editar", "Error en la edicion de los datos", 1, DateTime.UtcNow.Date, us);

        return View();
            }
        }
    [Authorize(Roles = "Administrador")]
    public ActionResult Eliminar(int id)
        {
            try
            {
                clsEstudiante ObjEstudiante = new clsEstudiante();
                bool Resultado = ObjEstudiante.EliminarEstudiante(id);

                if (Resultado)
                {
          int us = Convert.ToInt32(Session["IdUsuario"].ToString());
          Objbitacora.AgregarBitacora("EstudianteController", "Eliminar", "Exito", 1, DateTime.UtcNow.Date, us);

          return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Index");
                }

            }
            catch
            {
        int us = Convert.ToInt32(Session["IdUsuario"].ToString());
        Objbitacora.AgregarBitacora("EstudianteController", "Eliminar", "Error en la eliminacion de los datos", 1, DateTime.UtcNow.Date, us);

        return View();
            }
        }

    clsInstitucion Institucion = new clsInstitucion();
        clsUsuario Usuario = new clsUsuario();
        clsGrado Grado = new clsGrado();
        public List<ConsultarInstitucionResult> CargarInstitucion()
        {
            List<ConsultarInstitucionResult> instituciones = Institucion.ConsultarInstitucion();
            return instituciones;
        }

        public List<ConsultarUsuarioViewBagResult> CargarUsuario()
        {
            List<ConsultarUsuarioViewBagResult> usuario = Usuario.ConsultarUsuarioViewBag();
            return usuario;
        }

        public List<ConsultarGradoResult> CargarGrado()
        {
            List<ConsultarGradoResult> grado = Grado.ConsultarGrado();
            return grado;
        }
    }
}
