using BLL;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Proyecto.Controllers
{
  public class TipoOperacionController : Controller
  {
    clsBitacora Objbitacora = new clsBitacora();
    [Authorize(Roles = "Administrador, Docente")]
    public ActionResult Index()
    {
      try
      {
        List<TipoOperacion> listaTipoOpreracion = new List<TipoOperacion>();
        clsTipoOperacion TipoOperacion = new clsTipoOperacion();
        var data = TipoOperacion.ConsultarTipoOperacion().ToList();

        foreach (var item in data)
        {
          TipoOperacion modelo = new TipoOperacion();
          modelo.IdTipoOperacion = item.IdTipoOperacion;
          modelo.Descripcion = item.Descripcion;
          modelo.Estado = item.Estado;
          listaTipoOpreracion.Add(modelo);
        }
        return View(listaTipoOpreracion);
      }
      catch
      {
        int us = Convert.ToInt32(Session["IdUsuario"].ToString());
        Objbitacora.AgregarBitacora("TipoOperacionController", "Index", "Eror en la consulta de los datos", 1, DateTime.UtcNow.Date, us);

        return RedirectToAction("Error", "Error");
      }
    }
    [Authorize(Roles = "Administrador, Docente")]
    public ActionResult Crear()
    {
      try
      {
        ViewBag.ListaEstados = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");
        return View();
      }
      catch (Exception)
      {

        throw;
      }

    }


    [Authorize(Roles = "Administrador, Docente")]
    [HttpPost]
    public ActionResult Crear(TipoOperacion tipoOperacion)
    {
      try
      {
        if (ModelState.IsValid)
        {

          clsTipoOperacion TipoOperacion = new clsTipoOperacion();
          bool Resultado = TipoOperacion.AgregarTipoOperacion(tipoOperacion.Descripcion, tipoOperacion.Estado);
          clsBitacora Objbitacora = new clsBitacora();

          if (Resultado)
          {
            int us = Convert.ToInt32(Session["IdUsuario"].ToString());
            Objbitacora.AgregarBitacora("TipoOperacionController", "Crear", "Exito", 1, DateTime.UtcNow.Date, us);

            return RedirectToAction("Index");
          }
          else
          {
            ViewBag.ListaEstados = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");
            return View("Crear");
          }
        }
        else
        {
          ViewBag.ListaEstados = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");
          return View("Crear");
        }

      }
      catch
      {
        int us = Convert.ToInt32(Session["IdUsuario"].ToString());
        Objbitacora.AgregarBitacora("TipoOperacionController", "Crear", "Error en creacion de los datos", 1, DateTime.UtcNow.Date, us);

        return View();
      }
    }
    [Authorize(Roles = "Administrador, Docente")]
    public ActionResult Editar(int id)
    {
      try
      {
        clsTipoOperacion TipoOperacion = new clsTipoOperacion();

        ViewBag.ListaEstados = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");

        var dato = TipoOperacion.ConsultaTipoOperacion(id);

        TipoOperacion modelo = new TipoOperacion();
        modelo.IdTipoOperacion = dato.IdTipoOperacion;
        modelo.Descripcion = dato.Descripcion;
        modelo.Estado = dato.Estado;
        return View(modelo);
      }
      catch (Exception)
      {

        throw;
      }

    }
    [Authorize(Roles = "Administrador, Docente")]
    [HttpPost]
    public ActionResult Editar(int id, TipoOperacion tipoOperacion)
    {
      try
      {
        clsTipoOperacion TipoOperacion = new clsTipoOperacion();
        bool Resultado = TipoOperacion.ActualizarTipoOperacion(id, tipoOperacion.Descripcion, tipoOperacion.Estado);

        if (Resultado)
        {
          int us = Convert.ToInt32(Session["IdUsuario"].ToString());
          Objbitacora.AgregarBitacora("TipoOperacionController", "Editar", "Exito", 1, DateTime.UtcNow.Date, us);

          return RedirectToAction("Index");
        }
        else
        {
          ViewBag.ListaEstados = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");
          return View("Editar");
        }
      }
      catch
      {
        int us = Convert.ToInt32(Session["IdUsuario"].ToString());
        Objbitacora.AgregarBitacora("TipoOperacionController", "Editar", "Error en la edicion de los datos", 1, DateTime.UtcNow.Date, us);

        return View();
      }
    }
    [Authorize(Roles = "Administrador")]
    public ActionResult Eliminar(int id)
    {
      try
      {
        clsTipoOperacion TipoOperacion = new clsTipoOperacion();
        bool Resultado = TipoOperacion.EliminarTipoOperacion(id);

        if (Resultado)
        {
          int us = Convert.ToInt32(Session["IdUsuario"].ToString());
          Objbitacora.AgregarBitacora("TipoOperacionController", "Eliminar", "Exito", 1, DateTime.UtcNow.Date, us);

          return RedirectToAction("Index");
        }
        else
        {
          return RedirectToAction("Index");
        }

      }
      catch
      {
        int us = Convert.ToInt32(Session["IdUsuario"].ToString());
        Objbitacora.AgregarBitacora("TipoOperacionController", "Eliminar", "Error en la eliminacion de los datos", 1, DateTime.UtcNow.Date, us);

        return View();
      }
    }
  }
}
