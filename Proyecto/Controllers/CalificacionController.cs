using BLL;
using DAL;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Proyecto.Controllers
{
  
  public class CalificacionController : Controller
    {
[Authorize(Roles = "Administrador, Docente")]
    public ActionResult Index()
    {
      try
      {
        List<CalificacionList> listaCalificacion = new List<CalificacionList>();
        clsCalificacion Calificacion = new clsCalificacion();
        var data = Calificacion.ConsultarCalificacionLista().ToList();

        foreach (var item in data)
        {
          CalificacionList modelo = new CalificacionList();
          modelo.IdCalificacion = item.IdCalificacion;
          modelo.TipoOperacion = item.TipoOperacion;
          modelo.NombreEstudiante = item.NombreEstudiante;
          modelo.NombreDocente = item.NombreDocente;
          modelo.Materia = item.Materia;
          modelo.Nota = item.Nota;
          modelo.Fecha = item.Fecha;
          listaCalificacion.Add(modelo);
        }
        return View(listaCalificacion);
      }
      catch
      {
        string descMsg = "Error consultando la informacion de Calificacion";
        clsBitacora Objbitacora = new clsBitacora();
        int us = Convert.ToInt32(Session["IdUsuario"].ToString());
        Objbitacora.AgregarBitacora("CalificacionController", "Index", descMsg, 1, DateTime.UtcNow.Date, us);

        return RedirectToAction("Error", "Error");
      }
    }
    public ActionResult Crear()
    {
      try
      {
        ViewBag.ListaTipoOperacion = CargarTipoOperacion();
        ViewBag.ListaEstudiante = CargarEstudiante();
        ViewBag.ListaDocente = CargarDocente();
        ViewBag.ListaMateria = CargarMateria();
        return View();
      }
      catch (Exception)
      {
        string descMsg = "Error crear la informacion de Calificacion";
        clsBitacora Objbitacora = new clsBitacora();
        int us = Convert.ToInt32(Session["IdUsuario"].ToString());
        Objbitacora.AgregarBitacora("CalificacionController", "Crear", descMsg, 1, DateTime.UtcNow.Date, us);

        throw;
      }

    }

    [HttpPost]
    public ActionResult Crear(Calificacion calificacion)
    {
      try
      {
        if (ModelState.IsValid)
        {

          clsCalificacion Calificacion = new clsCalificacion();
          bool Resultado = Calificacion.AgregarCalificacion(calificacion.IdTipoOperacion, calificacion.IdEstudiante, calificacion.IdDocente,calificacion.IdMateria,calificacion.Nota,calificacion.Fecha);
          

          if (Resultado)
          {
            clsBitacora Objbitacora = new clsBitacora();
            int us = Convert.ToInt32(Session["IdUsuario"].ToString());
            Objbitacora.AgregarBitacora("CalificacionController", "Crear", "Sin registro", 1, DateTime.UtcNow.Date, us);

            return RedirectToAction("Index");
          }
          else
          {
            ViewBag.ListaTipoOperacion = CargarTipoOperacion();
            ViewBag.ListaEstudiante = CargarEstudiante();
            ViewBag.ListaDocente = CargarDocente();
            ViewBag.ListaMateria = CargarMateria();
            return View("Crear");
          }
        }
        else
        {
          ViewBag.ListaTipoOperacion = CargarTipoOperacion();
          ViewBag.ListaEstudiante = CargarEstudiante();
          ViewBag.ListaDocente = CargarDocente();
          ViewBag.ListaMateria = CargarMateria();
          return View("Crear");
        }

      }
      catch
      {
        return View();
      }
    }
    public ActionResult Editar(int id)
    {
      try
      {
        clsCalificacion Calificacion = new clsCalificacion();
        ViewBag.ListaTipoOperacion = CargarTipoOperacion();
        ViewBag.ListaEstudiante = CargarEstudiante();
        ViewBag.ListaDocente = CargarDocente();
        ViewBag.ListaMateria = CargarMateria();

        var dato = Calificacion.ConsultaCalificacion(id);

        Calificacion modelo = new Calificacion();
        modelo.IdCalificacion = dato.IdCalificacion;
        modelo.IdTipoOperacion = dato.IdTipoOperacion;
        modelo.IdEstudiante = dato.IdEstudiante;
        modelo.IdDocente = dato.IdDocente;
        modelo.IdMateria = dato.IdMateria;
        modelo.Nota = dato.Nota;
        modelo.Fecha = dato.Fecha;
        return View(modelo);
      }
      catch (Exception)
      {

        throw;
      }

    }
    [HttpPost]
    public ActionResult Editar(int id, Calificacion calificacion)
    {
      try
      {
        clsCalificacion Calificacion = new clsCalificacion();
        bool Resultado = Calificacion.ActualizarCalificacion(id, calificacion.IdTipoOperacion, calificacion.IdEstudiante, calificacion.IdDocente, calificacion.IdMateria,calificacion.Nota,calificacion.Fecha );

        if (Resultado)
        {
          clsBitacora Objbitacora = new clsBitacora();
          int us = Convert.ToInt32(Session["IdUsuario"].ToString());
          Objbitacora.AgregarBitacora("CalificacionController", "Editar", "Sin registro", 1, DateTime.UtcNow.Date, us);

          return RedirectToAction("Index");
        }
        else
        {
          ViewBag.ListaTipoOperacion = CargarTipoOperacion();
          ViewBag.ListaEstudiante = CargarEstudiante();
          ViewBag.ListaDocente = CargarDocente();
          ViewBag.ListaMateria = CargarMateria();

          string descMsg = "Error de edicion de la informacion de Calificacion";
          clsBitacora Objbitacora = new clsBitacora();
          int us = Convert.ToInt32(Session["IdUsuario"].ToString());
          Objbitacora.AgregarBitacora("CalificacionController", "Eliminar", descMsg, 1, DateTime.UtcNow.Date, us);

          return View("Editar");
        }
      }
      catch
      {
        return View();
      }
    }
    public ActionResult Eliminar(int id)
    {
      try
      {
        clsCalificacion Calificacion = new clsCalificacion();
        bool Resultado = Calificacion.EliminarCalificacion(id);

        if (Resultado)
        {
          clsBitacora Objbitacora = new clsBitacora();
          int us = Convert.ToInt32(Session["IdUsuario"].ToString());
          Objbitacora.AgregarBitacora("CalificacionController", "Eliminar", "Sin registro", 1, DateTime.UtcNow.Date, us);

          return RedirectToAction("Index");
        }
        else
        {
          string descMsg = "Error eliminacion de la informacion de Calificacion";
          clsBitacora Objbitacora = new clsBitacora();
          int us = Convert.ToInt32(Session["IdUsuario"].ToString());
          Objbitacora.AgregarBitacora("CalificacionController", "Eliminar", descMsg, 1, DateTime.UtcNow.Date, us);

          return RedirectToAction("Index");
        }

      }
      catch
      {
        return View();
      }
    }
    clsTipoOperacion TipoOperacion = new clsTipoOperacion();
    public List<ConsultarTipoOperacionResult> CargarTipoOperacion()
    {
      List<ConsultarTipoOperacionResult> tipoOperacion = TipoOperacion.ConsultarTipoOperacion();
      return tipoOperacion;
    }
    clsEstudiante Estudiante = new clsEstudiante();
    public List<ConsultarEstudianteResult> CargarEstudiante()
    {
      List<ConsultarEstudianteResult> estudiante = Estudiante.ConsultarEstudiante();
      return estudiante;
    }
    clsDocente Docente = new clsDocente();
    public List<ConsultarDocenteResult> CargarDocente()
    {
      List<ConsultarDocenteResult> docente = Docente.ConsultarDocente();
      return docente;
    }
    clsMateria Materia = new clsMateria();
    public List<ConsultarMateriaResult> CargarMateria()
    {
      List<ConsultarMateriaResult> materia = Materia.ConsultarMateria();
      return materia;
    }
  }
}
