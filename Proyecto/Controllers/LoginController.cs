using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL;
using Proyecto.Models;
using System.Web.Security;
using System.Configuration;
using Proyecto.Tools;

namespace Proyecto.Controllers
{
    public class LoginController : Controller
    {
        clsUsuario ObjUsuario = new clsUsuario();

        [AllowAnonymous]
        // GET: Login
        
        public ActionResult Login()
        {
      if (Session["Identificacion"] != null)
      {
        clsBitacora Objbitacora = new clsBitacora();
        int us = Convert.ToInt32(Session["IdUsuario"].ToString());
        Objbitacora.AgregarBitacora("LoginController", "Login", "Sin registro", 1, DateTime.UtcNow.Date, us);

        return RedirectToAction("Index", "Home");
      }

      return View();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(Login login)
        {

            var SecretKey = ConfigurationManager.AppSettings["SecretKey"];
            if (ModelState.IsValid)
            {
                var ClaveEncriptada = Seguridad.EncryptString(SecretKey, login.Clave);
               var ClaveDesencriptada = Seguridad.DecryptString(SecretKey, ClaveEncriptada);
                var dato = ObjUsuario.ValidaUsuario(login.Correo,ClaveEncriptada);

                if (dato==null)
                {
                    return View(login);
                }
                else
                {
          var ListaRoles = ObjUsuario.ConsultarRolesUsuario(dato.IdUsuario);
          var roles = String.Join(",", ListaRoles.Select(x => x.Rol));
                    clsRol role = new clsRol();
                    var rol = role.ConsultaRolPorUsuario(dato.Correo);
                    Session["Identificacion"] = dato.Identificacion.ToString();
                    Session["IdInstitucion"] = dato.IdInstitucion.ToString();
                    Session["IdUsuario"] = dato.IdUsuario.ToString();
                    Session["Roles"] = rol;

          var username = dato.Correo;
          FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1, username, DateTime.Now, DateTime.Now.AddMinutes(30), login.Recordarme, roles, FormsAuthentication.FormsCookiePath);
          string hash = FormsAuthentication.Encrypt(ticket);
          HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, hash);
          Response.Cookies.Add(cookie);
          if (!Request.Browser.IsMobileDevice)
                    {
            clsBitacora Objbitacora = new clsBitacora();
            int us = Convert.ToInt32(Session["IdUsuario"].ToString());
            Objbitacora.AgregarBitacora("LoginController", "Home", "Sin registro", 1, DateTime.UtcNow.Date, us);

            return RedirectToAction("Index", "Home");
                    }
                    else
                    {
            clsBitacora Objbitacora = new clsBitacora();
            int us = Convert.ToInt32(Session["IdUsuario"].ToString());
            Objbitacora.AgregarBitacora("LoginController", "Carnet", "Sin registro", 1, DateTime.UtcNow.Date, us);

            return RedirectToAction("Index", "Cartent");
                    }

                }
            }
            else
            {
                return View(login);
            }
        }
        /// <summary>
        /// ESte Metodo sirve para salir del sistema
        /// </summary>
        /// <returns></returns>
        [Authorize]
        public ActionResult Salir()
        {
            Session.Remove("Identificacion");
            Session.RemoveAll();
            Response.Cache.SetCacheability(HttpCacheability.Private);
            Session.Clear();
            FormsAuthentication.SignOut();
            Session.Abandon();
            Response.Cache.SetNoServerCaching();
            Request.Cookies.Clear();
            return RedirectToAction("Login", "Login");
        }
    }
}
