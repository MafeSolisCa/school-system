using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAL;
using BLL;
using Proyecto.Models;

namespace Proyecto.Controllers
{
    [Authorize]
  
  public class RolController : Controller
    {
    clsBitacora Objbitacora = new clsBitacora();
    [Authorize(Roles = "Administrador")]
    public ActionResult Index()
        {
            try
            {
                List<Rol> listaRol = new List<Rol>();
                clsRol Rol = new clsRol();
                var data = Rol.ConsultarRol().ToList();

                foreach (var item in data)
                {
                    Rol modelo = new Rol();
                    modelo.IdRol = item.IdRol;
                    modelo.Descripcion = item.Descripcion;
                    modelo.Estado = item.Estado;
                    listaRol.Add(modelo);
                }
                return View(listaRol);
            }
            catch
            {
        int us = Convert.ToInt32(Session["IdUsuario"].ToString());
        Objbitacora.AgregarBitacora("RolController", "Index", "Eror en la consulta de los datos", 1, DateTime.UtcNow.Date, us);

        return RedirectToAction("Error", "Error");
            }
        }
    [Authorize(Roles = "Administrador")]
    public ActionResult Crear()
        {
            try
            {
                ViewBag.ListaEstados = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");
                return View();
            }
            catch (Exception)
            {

                throw;
            }

        }

    [Authorize(Roles = "Administrador")]
    [HttpPost]
        public ActionResult Crear(Rol rol)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    clsRol Rol = new clsRol();
                    bool Resultado = Rol.AgregarRol(rol.Descripcion, rol.Estado);
                    clsBitacora Objbitacora = new clsBitacora();

                    if (Resultado)
                    {
            int us = Convert.ToInt32(Session["IdUsuario"].ToString());
            Objbitacora.AgregarBitacora("RolController", "Crear", "Exito", 1, DateTime.UtcNow.Date, us);

            return RedirectToAction("Index");
                    }
                    else
                    {
                        ViewBag.ListaEstados = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");
                        return View("Crear");
                    }
                }
                else
                {
                    ViewBag.ListaEstados = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");
                    return View("Crear");
                }

            }
            catch
            {
        int us = Convert.ToInt32(Session["IdUsuario"].ToString());
        Objbitacora.AgregarBitacora("RolController", "Crear", "Error en creacion de los datos", 1, DateTime.UtcNow.Date, us);

        return View();
            }
        }
    [Authorize(Roles = "Administrador")]
    public ActionResult Editar(int id)
        {
            try
            {
                clsRol Rol = new clsRol();
        ViewBag.ListaEstados = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");
        var dato = Rol.ConsultaRol(id);

                Rol modelo = new Rol();
                modelo.IdRol = dato.IdRol;
                modelo.Descripcion = dato.Descripcion;
                modelo.Estado = dato.Estado;
                return View(modelo);
            }
            catch (Exception)
            {

                throw;
            }

        }
    [Authorize(Roles = "Administrador")]
    [HttpPost]
        public ActionResult Editar(int id, Rol rol)
        {
            try
            {
                clsRol Rol = new clsRol();
                bool Resultado = Rol.ActualizarRol(id, rol.Descripcion, rol.Estado);

                if (Resultado)
                {
          int us = Convert.ToInt32(Session["IdUsuario"].ToString());
          Objbitacora.AgregarBitacora("RolController", "Editar", "Exito", 1, DateTime.UtcNow.Date, us);

          return RedirectToAction("Index");
                }
                else
                {
                    ViewBag.ListaEstados = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");
                    return View("Editar");
                }
            }
            catch
            {
        int us = Convert.ToInt32(Session["IdUsuario"].ToString());
        Objbitacora.AgregarBitacora("RolController", "Editar", "Error en la edicion de los datos", 1, DateTime.UtcNow.Date, us);

        return View();
            }
        }
    [Authorize(Roles = "Administrador")]
    public ActionResult Eliminar(int id)
        {
            try
            {
                clsRol Rol = new clsRol();
                bool Resultado = Rol.EliminarRol(id);

                if (Resultado)
                {
          int us = Convert.ToInt32(Session["IdUsuario"].ToString());
          Objbitacora.AgregarBitacora("RolController", "Eliminar", "Exito", 1, DateTime.UtcNow.Date, us);

          return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Index");
                }

            }
            catch
            {
        int us = Convert.ToInt32(Session["IdUsuario"].ToString());
        Objbitacora.AgregarBitacora("RolController", "Eliminar", "Error en la eliminacion de los datos", 1, DateTime.UtcNow.Date, us);

        return View();
            }
        }
    }
}
