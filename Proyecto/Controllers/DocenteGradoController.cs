using BLL;
using DAL;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Proyecto.Controllers
{
    public class DocenteGradoController : Controller
    {
    clsDocente ObjDocente = new clsDocente();
    clsBitacora Objbitacora = new clsBitacora();
    public ActionResult Index()
    {
      try
      {
        List<DocenteGradoList> listaDocenteGrado = new List<DocenteGradoList>();
        clsDocenteGrado DocenteGrado = new clsDocenteGrado();
        var data = DocenteGrado.ConsultarDocenteGradoLista().ToList();

        foreach (var item in data)
        {
          DocenteGradoList modelo = new DocenteGradoList();
          modelo.IdDocenteGrado = item.IdDocenteGrado;
          modelo.NombreDocente = item.NombreDocente;
          modelo.Grado = item.Grado;
          listaDocenteGrado.Add(modelo);
        }
        return View(listaDocenteGrado);
      }
      catch
      {
        int us = Convert.ToInt32(Session["IdUsuario"].ToString());
        Objbitacora.AgregarBitacora("DocenteGradoController", "Index", "Error al consultar los datos", 1, DateTime.UtcNow.Date, us);

        return RedirectToAction("Error", "Error");
      }

    }
    public ActionResult Crear()
    {
      try
      {
        ViewBag.ListaDocente = CargarDocente();
        ViewBag.ListaGrado = CargarGrado();
        ViewBag.ListaEstados = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");

        return View();
      }
      catch (Exception)
      {
  
        throw;
      }

    }


    [HttpPost]
    public ActionResult Crear(DocenteGrado docenteGrado)
    {
      try
      {
        if (ModelState.IsValid)
        {

          clsDocenteGrado DocenteGrado = new clsDocenteGrado();
          bool Resultado = DocenteGrado.AgregarDocenteGrado(docenteGrado.IdDocente, docenteGrado.IdGrado);

          if (Resultado)
          {
            int us = Convert.ToInt32(Session["IdUsuario"].ToString());
            Objbitacora.AgregarBitacora("DocenteGradoController", "Crear", "Exito", 1, DateTime.UtcNow.Date, us);

            return RedirectToAction("Index");
          }
          else
          {
            ViewBag.ListaDocente = CargarDocente();
            ViewBag.ListaGrado = CargarGrado();
            ViewBag.ListaEstados = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");

            return View("Crear");
          }
        }
        else
        {
          ViewBag.ListaDocente = CargarDocente();
          ViewBag.ListaGrado = CargarGrado();
          ViewBag.ListaEstados = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");

          return View("Crear");
        }

      }
      catch
      {
        int us = Convert.ToInt32(Session["IdUsuario"].ToString());
        Objbitacora.AgregarBitacora("DocenteGradoController", "Crear", "Error en la creacion de los datos", 1, DateTime.UtcNow.Date, us);

        return View();
      }
    }

    public ActionResult Editar(int id)
    {
      try
      {
        clsDocenteGrado DocenteGrado = new clsDocenteGrado();
        ViewBag.ListaDocente = CargarDocente();
        ViewBag.ListaGrado = CargarGrado();
        ViewBag.ListaEstados = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");
        var dato = DocenteGrado.ConsultaDocenteGrado(id);

        DocenteGrado modelo = new DocenteGrado();
        modelo.IdDocenteGrado = dato.IdDocenteGrado;
        modelo.IdDocente = dato.IdDocente;
        modelo.IdGrado = dato.IdGrado;
        return View(modelo);
      }
      catch (Exception)
      {

        throw;
      }

    }
    // POST: Docente/Editar/5
    [HttpPost]
    public ActionResult Editar(int id, DocenteGrado docenteGrado)
    {
      try
      {

        clsDocenteGrado DocenteGrado = new clsDocenteGrado();
        bool Resultado = DocenteGrado.ActualizarDocenteGrado(id, docenteGrado.IdDocente, docenteGrado.IdGrado);

        if (Resultado)
        {
          int us = Convert.ToInt32(Session["IdUsuario"].ToString());
          Objbitacora.AgregarBitacora("DocenteGradoController", "Editar", "Exito", 1, DateTime.UtcNow.Date, us);

          return RedirectToAction("Index");
        }
        else
        {

          ViewBag.ListaDocente = CargarDocente();
          ViewBag.ListaGrado = CargarGrado();
          ViewBag.ListaEstados = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");
          return View("Editar");
        }
      }
      catch
      {
        int us = Convert.ToInt32(Session["IdUsuario"].ToString());
        Objbitacora.AgregarBitacora("DocenteGradoController", "Editar", "Error en la edicion de los datos", 1, DateTime.UtcNow.Date, us);

        return View();
      }
    }
    // POST: Docente/Eliminar/5
    public ActionResult Eliminar(int id)
    {
      try
      {
        clsDocenteGrado ObjDocenteGrado = new clsDocenteGrado();
        bool Resultado = ObjDocenteGrado.EliminarDocenteGrado(id);

        if (Resultado)
        {
          int us = Convert.ToInt32(Session["IdUsuario"].ToString());
          Objbitacora.AgregarBitacora("DocenteGradoController", "Eliminar", "Exito", 1, DateTime.UtcNow.Date, us);

          return RedirectToAction("Index");
        }
        else
        {

          return RedirectToAction("Index");
        }

      }
      catch
      {
        int us = Convert.ToInt32(Session["IdUsuario"].ToString());
        Objbitacora.AgregarBitacora("DocenteGradoController", "Eliminar", "Error en la eliminacion", 1, DateTime.UtcNow.Date, us);

        return View();
      }
    }

    clsDocente Docente = new clsDocente();
    public List<ConsultarDocenteGradoVBResult> CargarDocente()
    {
      List<ConsultarDocenteGradoVBResult> docente = Docente.ConsultarDocenteGradoVB();
      return docente;
    }
    clsGrado Grado = new clsGrado();
    public List<ConsultarGradoResult> CargarGrado()
    {
      List<ConsultarGradoResult> grado = Grado.ConsultarGrado();
      return grado;
    }
  }
}
