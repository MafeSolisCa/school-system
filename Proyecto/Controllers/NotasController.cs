using BLL;
using DAL;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Proyecto.Controllers
{
  [Authorize]
    public class NotasController : Controller
    {
    clsBitacora Objbitacora = new clsBitacora();
    clsDocente ObjDocente = new clsDocente();
    [Authorize(Roles = "Administrador, Docente, Estudiante")]
    public ActionResult Consulta(int id)
        {
      try
      {
        clsNota Nota = new clsNota();

        var dato = Nota.ConsultaNotaLista(id);

        NotaList modelo = new NotaList();
        modelo.IdNota = dato.IdNota;
        modelo.Identificacion = dato.Identificacion;
        modelo.Nombre = dato.Nombre;
        modelo.Materia = dato.Materia;
        modelo.Nota = dato.Nota;
        modelo.Estado = dato.Estado;
        return View(modelo);
      }
      catch (Exception)
      {

        throw;
      }
    }
    [Authorize(Roles = "Administrador, Docente, Estudiante")]
    public ActionResult Index()
    {
      try
      {
        List<NotaList> listaRol = new List<NotaList>();
        clsNota Rol = new clsNota();
        var data = Rol.ConsultarNotaLista().ToList();

        foreach (var item in data)
        {
          NotaList modelo = new NotaList();
          modelo.IdNota = item.IdNota;
          modelo.Nombre = item.Nombre;
          modelo.Materia = item.Materia;
          modelo.Nota = item.Nota;
          modelo.Estado = item.Estado;
          listaRol.Add(modelo);
        }
        return View(listaRol);
      }
      catch
      {
        int us = Convert.ToInt32(Session["IdUsuario"].ToString());
        Objbitacora.AgregarBitacora("NotasController", "Index", "Eror en la consulta de los datos", 1, DateTime.UtcNow.Date, us);

        return RedirectToAction("Error", "Error");
      }
    }
    [Authorize(Roles = "Administrador, Docente")]
    public ActionResult Crear()
    {
      try
      {
        ViewBag.ListaEstudiante = CargarEstudiante();
        ViewBag.ListaMateria = CargarMateria();
        ViewBag.ListaEstados = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");
        return View();
      }
      catch (Exception)
      {

        throw;
      }

    }

    [Authorize(Roles = "Administrador, Docente")]
    [HttpPost]
    public ActionResult Crear(Notas notas)
    {
      try
      {
        if (ModelState.IsValid)
        {

          clsNota Notas = new clsNota();
          bool Resultado = Notas.AgregarNota(notas.IdEstudiante, notas.IdMateria, notas.Nota, notas.Estado);
          clsBitacora Objbitacora = new clsBitacora();

          if (Resultado)
          {
            int us = Convert.ToInt32(Session["IdUsuario"].ToString());
            Objbitacora.AgregarBitacora("NotasController", "Crear", "Exito", 1, DateTime.UtcNow.Date, us);

            return RedirectToAction("Index");
          }
          else
          {
            ViewBag.ListaEstudiante = CargarEstudiante();
            ViewBag.ListaMateria = CargarMateria();
            ViewBag.ListaEstados = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");
            return View("Crear");
          }
        }
        else
        {
          ViewBag.ListaEstudiante = CargarEstudiante();
          ViewBag.ListaMateria = CargarMateria();
          ViewBag.ListaEstados = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");
          return View("Crear");
        }

      }
      catch
      {
        int us = Convert.ToInt32(Session["IdUsuario"].ToString());
        Objbitacora.AgregarBitacora("NotasController", "Crear", "Error en creacion de los datos", 1, DateTime.UtcNow.Date, us);

        return View();
      }
    }
    [Authorize(Roles = "Administrador, Docente")]
    public ActionResult Editar(int id)
    {
      try
      {
        clsNota Nota = new clsNota();
        ViewBag.ListaEstudiante = CargarEstudiante();
        ViewBag.ListaMateria = CargarMateria();
        ViewBag.ListaEstados = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");
        var dato = Nota.ConsultaNota(id);

        Notas modelo = new Notas();
        modelo.IdNota = dato.IdNota;
        modelo.IdEstudiante = dato.IdEstudiante;
        modelo.IdMateria = dato.IdMateria;
        modelo.Nota = dato.Nota;
        modelo.Estado = dato.Estado;
        return View(modelo);
      }
      catch (Exception)
      {

        throw;
      }

    }
    [Authorize(Roles = "Administrador, Docente")]
    [HttpPost]
    public ActionResult Editar(int id, Notas notas)
    {
      try
      {
        clsNota Nota = new clsNota();
        bool Resultado = Nota.ActualizarNota(id, notas.IdEstudiante, notas.IdMateria, notas.Nota, notas.Estado);

        if (Resultado)
        {
          int us = Convert.ToInt32(Session["IdUsuario"].ToString());
          Objbitacora.AgregarBitacora("NotasController", "Editar", "Exito", 1, DateTime.UtcNow.Date, us);

          return RedirectToAction("Index");
        }
        else
        {
          ViewBag.ListaEstudiante = CargarEstudiante();
          ViewBag.ListaMateria = CargarMateria();
          ViewBag.ListaEstados = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");
          return View("Editar");
        }
      }
      catch
      {
        int us = Convert.ToInt32(Session["IdUsuario"].ToString());
        Objbitacora.AgregarBitacora("NotasController", "Editar", "Error en la edicion de los datos", 1, DateTime.UtcNow.Date, us);

        return View();
      }
    }
    [Authorize(Roles = "Administrador, Docente")]
    public ActionResult Eliminar(int id)
    {
      try
      {
        clsNota Nota = new clsNota();
        bool Resultado = Nota.EliminarNota(id);

        if (Resultado)
        {
          int us = Convert.ToInt32(Session["IdUsuario"].ToString());
          Objbitacora.AgregarBitacora("NotasController", "Eliminar", "Exito", 1, DateTime.UtcNow.Date, us);

          return RedirectToAction("Index");
        }
        else
        {
          return RedirectToAction("Index");
        }

      }
      catch
      {
        int us = Convert.ToInt32(Session["IdUsuario"].ToString());
        Objbitacora.AgregarBitacora("MateriasController", "Eliminar", "Error en la eliminacion de los datos", 1, DateTime.UtcNow.Date, us);

        return View();
      }
    }
    clsEstudiante Estudiante = new clsEstudiante();
    public List<ConsultarEstudianteResult> CargarEstudiante()
    {
      List<ConsultarEstudianteResult> estudiante = Estudiante.ConsultarEstudiante();
      return estudiante;
    }

    clsMateria Materia = new clsMateria();
    public List<ConsultarMateriaResult> CargarMateria()
    {
      List<ConsultarMateriaResult> materia = Materia.ConsultarMateria();
      return materia;
    }

  }
}
