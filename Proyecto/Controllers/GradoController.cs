using BLL;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Proyecto.Controllers
{
    public class GradoController : Controller
    {
    clsDocente ObjDocente = new clsDocente();
    clsBitacora Objbitacora = new clsBitacora();
    [Authorize(Roles = "Administrador")]
    public ActionResult Index()
    {
      try
      {
        List<Grado> listaGrado = new List<Grado>();
        clsGrado grado = new clsGrado();
        var data = grado.ConsultarGrado().ToList();

        foreach (var item in data)
        {
          Grado modelo = new Grado();
          modelo.IdGrado = item.IdGrado;
          modelo.Descripcion = item.Descripcion;
          modelo.Estado = item.Estado;
          listaGrado.Add(modelo);
        }
        return View(listaGrado);
      }
      catch
      {
        int us = Convert.ToInt32(Session["IdUsuario"].ToString());
        Objbitacora.AgregarBitacora("GradoController", "Index", "Error en la creacion de los datos", 1, DateTime.UtcNow.Date, us);

        return RedirectToAction("Error", "Error");
      }
    }
    public ActionResult Crear()
    {
      try
      {
        ViewBag.ListaEstados = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");
        return View();
      }
      catch (Exception)
      {

        throw;
      }

    }

    [HttpPost]
    public ActionResult Crear(Grado grado)
    {
      try
      {
        if (ModelState.IsValid)
        {

          clsGrado Grado = new clsGrado();
          bool Resultado = Grado.AgregarGrado(grado.Descripcion, grado.Estado);

          if (Resultado)
          {
            int us = Convert.ToInt32(Session["IdUsuario"].ToString());
            Objbitacora.AgregarBitacora("GradoController", "Crear", "Exito", 1, DateTime.UtcNow.Date, us);

            return RedirectToAction("Index");
          }
          else
          {
            ViewBag.ListaEstados = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");
            return View("Crear");
          }
        }
        else
        {
          ViewBag.ListaEstados = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");
          return View("Crear");
        }

      }
      catch
      {
        int us = Convert.ToInt32(Session["IdUsuario"].ToString());
        Objbitacora.AgregarBitacora("GradoController", "Crear", "Error en la creacion de los datos", 1, DateTime.UtcNow.Date, us);

        return View();
      }
    }
    public ActionResult Editar(int id)
    {
      try
      {
        clsGrado Grado = new clsGrado();
        ViewBag.ListaEstados = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");
        var dato = Grado.ConsultaGrado(id);

        Grado modelo = new Grado();
        modelo.IdGrado = dato.IdGrado;
        modelo.Descripcion = dato.Descripcion;
        modelo.Estado = dato.Estado;
        return View(modelo);
      }
      catch (Exception)
      {

        throw;
      }

    }
    [HttpPost]
    public ActionResult Editar(int id, Grado grado)
    {
      try
      {

        clsGrado Grado = new clsGrado();
        bool Resultado = Grado.ActualizarGrado(id, grado.Descripcion, grado.Estado);

        if (Resultado)
        {
          int us = Convert.ToInt32(Session["IdUsuario"].ToString());
          Objbitacora.AgregarBitacora("GradoController", "Editar", "Exito", 1, DateTime.UtcNow.Date, us);

          return RedirectToAction("Index");
        }
        else
        {
          ViewBag.ListaEstados = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");
          return View("Editar");
        }
      }
      catch
      {
        int us = Convert.ToInt32(Session["IdUsuario"].ToString());
        Objbitacora.AgregarBitacora("GradoController", "Editar", "Eroro en la edicion de los datos", 1, DateTime.UtcNow.Date, us);

        return View();
      }
    }
    public ActionResult Eliminar(int id)
    {
      try
      {
        clsGrado Grado = new clsGrado();
        bool Resultado = Grado.EliminarGrado(id);

        if (Resultado)
        {
          int us = Convert.ToInt32(Session["IdUsuario"].ToString());
          Objbitacora.AgregarBitacora("GradoController", "Eliminar", "Exito", 1, DateTime.UtcNow.Date, us);

          return RedirectToAction("Index");
        }
        else
        {
          return RedirectToAction("Index");
        }

      }
      catch
      {
        int us = Convert.ToInt32(Session["IdUsuario"].ToString());
        Objbitacora.AgregarBitacora("GradoController", "Eliminar", "Eror en la eliminacion de los datos", 1, DateTime.UtcNow.Date, us);

        return View();
      }
    }
  }
}
