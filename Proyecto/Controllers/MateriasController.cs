using BLL;
using DAL;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Proyecto.Controllers
{
    public class MateriasController : Controller
    {
    clsBitacora Objbitacora = new clsBitacora();
    clsDocente ObjDocente = new clsDocente();
    [Authorize(Roles = "Administrador")]
    public ActionResult Index()
        {
      try
      {
        List<MateriaList> listaMateria = new List<MateriaList>();
        clsMateria Rol = new clsMateria();
        var data = Rol.ConsultarMateriaLista().ToList();

        foreach (var item in data)
        {
          MateriaList modelo = new MateriaList();
          modelo.IdMateria = item.IdMateria;
          modelo.Descripcion = item.Descripcion;
          modelo.Institucion = item.Institucion;
          modelo.Estado = item.Estado;
          listaMateria.Add(modelo);
        }
        return View(listaMateria);
      }
      catch
      {
        int us = Convert.ToInt32(Session["IdUsuario"].ToString());
        Objbitacora.AgregarBitacora("MateriasController", "Index", "Eror en la consulta de los datos", 1, DateTime.UtcNow.Date, us);

        return RedirectToAction("Error", "Error");
      }
    }

    public ActionResult Materias()
    {
      try
      {
        List<MateriasPorEstudiante> listaMateriasPorEstudiante = new List<MateriasPorEstudiante>();
        clsMateria Materia = new clsMateria();
        int Usuario = Convert.ToInt32(Session["IdUsuario"]);
        var data = Materia.ConsultaMateriasPorEstudiante(Usuario).ToList();

        foreach (var item in data)
        {
          MateriasPorEstudiante modelo = new MateriasPorEstudiante();
          modelo.Descripcion = item.Descripcion;
          listaMateriasPorEstudiante.Add(modelo);
        }
        return View(listaMateriasPorEstudiante);
      }
      catch
      {
        int us = Convert.ToInt32(Session["IdUsuario"].ToString());
        Objbitacora.AgregarBitacora("MateriasController", "Materias", "Eror en la consulta de los datos", 1, DateTime.UtcNow.Date, us);

        return RedirectToAction("Error", "Error");
      }
    }
    [Authorize(Roles = "Administrador")]
    public ActionResult Crear()
    {
      try
      {
        ViewBag.Institucion = CargarInstitucion();
        ViewBag.ListaEstados = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");
        return View();
      }
      catch (Exception)
      {

        throw;
      }

    }

    [Authorize(Roles = "Administrador")]
    [HttpPost]
    public ActionResult Crear(Materia materia)
    {
      try
      {
        if (ModelState.IsValid)
        {

          clsMateria Materia = new clsMateria();
          bool Resultado = Materia.AgregarMateria(materia.Descripcion, materia.IdInstitucion, materia.Estado);
         
          clsBitacora Objbitacora = new clsBitacora();

          if (Resultado)
          {
            int us = Convert.ToInt32(Session["IdUsuario"].ToString());
            Objbitacora.AgregarBitacora("MateriasController", "Crear", "Exito", 1, DateTime.UtcNow.Date, us);

            return RedirectToAction("Index");
          }
          else
          {
            ViewBag.Institucion = CargarInstitucion();
            ViewBag.ListaEstados = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");
            
            return View("Crear");
          }
        }
        else
        {
          ViewBag.Institucion = CargarInstitucion();
          ViewBag.ListaEstados = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");
          
          return View("Crear");
        }

      }
      catch
      {
        int us = Convert.ToInt32(Session["IdUsuario"].ToString());
        Objbitacora.AgregarBitacora("MateriasController", "Crear", "Error en creacion de los datos", 1, DateTime.UtcNow.Date, us);

        return View();
      }
    }
    [Authorize(Roles = "Administrador")]
    public ActionResult Editar(int id)
    {
      try
      {
        clsMateria materia = new clsMateria();
        ViewBag.Institucion = CargarInstitucion();
        ViewBag.ListaEstados = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");
        var dato = materia.ConsultaMateria(id);

        Materia modelo = new Materia();
        modelo.IdMateria = dato.IdMateria;
        modelo.Descripcion = dato.Descripcion;
        modelo.IdInstitucion = Convert.ToInt32(dato.IdInstitucion);
        modelo.Estado = dato.Estado;
        return View(modelo);
      }
      catch (Exception)
      {

        throw;
      }

    }
    [Authorize(Roles = "Administrador")]
    [HttpPost]
    public ActionResult Editar(int id, Materia materia)
    {
      try
      {
        clsMateria Materia = new clsMateria();
        bool Resultado = Materia.ActualizarMateria(id, materia.Descripcion, materia.IdInstitucion, materia.Estado);

        if (Resultado)
        {
          int us = Convert.ToInt32(Session["IdUsuario"].ToString());
          Objbitacora.AgregarBitacora("MateriasController", "Editar", "Exito", 1, DateTime.UtcNow.Date, us);

          return RedirectToAction("Index");
        }
        else
        {
          ViewBag.Institucion = CargarInstitucion();
          ViewBag.ListaEstados = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");
          
          return View("Editar");
        }
      }
      catch
      {
        int us = Convert.ToInt32(Session["IdUsuario"].ToString());
        Objbitacora.AgregarBitacora("MateriasController", "Editar", "Error en la edicion de los datos", 1, DateTime.UtcNow.Date, us);

        return View();
      }
    }
    [Authorize(Roles = "Administrador")]
    public ActionResult Eliminar(int id)
    {
      try
      {
        clsMateria Materia = new clsMateria();
        bool Resultado = Materia.EliminarMateria(id);

        if (Resultado)
        {
          int us = Convert.ToInt32(Session["IdUsuario"].ToString());
          Objbitacora.AgregarBitacora("MateriasController", "Eliminar", "Exito", 1, DateTime.UtcNow.Date, us);

          return RedirectToAction("Index");
        }
        else
        {
          return RedirectToAction("Index");
        }

      }
      catch
      {
        int us = Convert.ToInt32(Session["IdUsuario"].ToString());
        Objbitacora.AgregarBitacora("MateriasController", "Eliminar", "Error en la eliminacion de los datos", 1, DateTime.UtcNow.Date, us);

        return View();
      }
    }
    clsInstitucion Institucion = new clsInstitucion();
    public List<ConsultarInstitucionResult> CargarInstitucion()
    {
      List<ConsultarInstitucionResult> instituciones = Institucion.ConsultarInstitucion();
      return instituciones;
    }
  }
}
