using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL;
using DAL;
using System.Web.Security;
using System.Configuration;
using Proyecto.Models;
using Proyecto.Tools;

namespace Proyecto.Controllers
{
    [Authorize]
    public class UsuarioController : Controller
    {
    clsBitacora Objbitacora = new clsBitacora();
    [Authorize(Roles = "Administrador")]
    /// <summary>
    /// Get: Usuario
    /// Realiza una consulta a la base de 
    /// datos de un proceso almacenado con datos espesificos
    /// </summary>
    /// <returns></returns>
    /// 
    public ActionResult Index()
        {
            try
            {
                List<UsuarioLista> listaUsuario = new List<UsuarioLista>();
                clsUsuario Usuario = new clsUsuario();
                var data = Usuario.ConsultarUsuarioLista().ToList();

                foreach (var item in data)
                {
                    UsuarioLista modelo = new UsuarioLista();
                    modelo.IdUsuario = item.IdUsuario;
                    modelo.Institucion = item.Institucion;
                    modelo.TipoIdentificacion = item.TipoIdentificacion;
                    modelo.Identificacion = item.Identificacion;
                    modelo.Nombre = item.Nombre;          
                    modelo.Correo = item.Correo;          
                    modelo.Estado = item.Estado;
                    listaUsuario.Add(modelo);
                }
                return View(listaUsuario);
            }
            catch
            {
        int us = Convert.ToInt32(Session["IdUsuario"].ToString());
        Objbitacora.AgregarBitacora("UsuarioController", "Index", "Eror en la consulta de los datos", 1, DateTime.UtcNow.Date, us);

        return RedirectToAction("Error", "Error");

            }
        }
    [Authorize(Roles = "Administrador")]
    public ActionResult Crear()
        {
            try
            {
                ViewBag.ListaInstitucion = CargarInstitucion();
                ViewBag.ListaTipoIdentificacion = CargarTipoIdentificacion();
                ViewBag.ListaProvincias = CargaProvincias();
                ViewBag.ListaEstado = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");

                return View();
            }
            catch (Exception)
            {

                throw;
            }

        }
    [Authorize(Roles = "Administrador")]
    /// <summary>
    /// Metodo Post que agrega un usuario y el rol por medio de un proceso almacenado en la base de datos
    /// </summary>
    /// <param name="usuario"></param>
    /// <returns></returns>
    [HttpPost]
        public ActionResult Crear(Usuario usuario)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var SecretKey = ConfigurationManager.AppSettings["SecretKey"];
                    var ClaveEncriptada = Seguridad.EncryptString(SecretKey, usuario.Password);
                    clsUsuario ObjUsuario = new clsUsuario();
                    bool Resultado = ObjUsuario.AgregarUsuario(usuario.IdInstitucion, usuario.IdTipoIdentificacion, 
                        usuario.Identificacion, usuario.Nombre, usuario.Apellido1, usuario.Apellido2, ClaveEncriptada, 
                        usuario.Telefono, usuario.Direccion, usuario.Correo, usuario.FechaNacimiento,usuario.Estado, usuario.Provincia, usuario.Canton, usuario.Distrito);


                    if (Resultado)
                    {
            int us = Convert.ToInt32(Session["IdUsuario"].ToString());
            Objbitacora.AgregarBitacora("UsuarioController", "Crear", "Exito", 1, DateTime.UtcNow.Date, us);

            return RedirectToAction("Index");
                    }
                    else
                    {

            ViewBag.ListaInstitucion = CargarInstitucion();
            ViewBag.ListaTipoIdentificacion = CargarTipoIdentificacion();
            ViewBag.ListaProvincias = CargaProvincias();
            ViewBag.ListaEstado = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");
            return View("Crear");
                    }
                }
                else
                {
          ViewBag.ListaInstitucion = CargarInstitucion();
          ViewBag.ListaTipoIdentificacion = CargarTipoIdentificacion();
          ViewBag.ListaProvincias = CargaProvincias();
          ViewBag.ListaEstado = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");

          return View("Crear");
                }


            }
            catch
            {
        int us = Convert.ToInt32(Session["IdUsuario"].ToString());
        Objbitacora.AgregarBitacora("UsuarioController", "Crear", "Error en creacion de los datos", 1, DateTime.UtcNow.Date, us);

        return View();
            }
        }
    [Authorize(Roles = "Administrador")]
    public ActionResult Editar(int id)
        {
            try
            {
                clsUsuario usuario = new clsUsuario();
        ViewBag.ListaInstitucion = CargarInstitucion();
        ViewBag.ListaTipoIdentificacion = CargarTipoIdentificacion();
        ViewBag.ListaProvincias = CargaProvincias();
        ViewBag.ListaEstado = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");
        var SecretKey = ConfigurationManager.AppSettings["SecretKey"];

                var dato = usuario.ConsultaUsuario(id);

                Usuario modelo = new Usuario();
                modelo.IdUsuario = dato.IdUsuario;
                modelo.IdInstitucion = dato.IdInstitucion;
                modelo.IdTipoIdentificacion = dato.IdTipoIdentificacion;
                modelo.Identificacion = dato.Identificacion;
                modelo.Nombre = dato.Nombre;
                modelo.Apellido1 = dato.Apellido1;
                modelo.Apellido2 = dato.Apellido2;
                modelo.Password = Seguridad.DecryptString(SecretKey, dato.Password);
                modelo.Telefono = dato.Telefono;
                modelo.Direccion = dato.Direccion;
                modelo.Correo = dato.Correo;
                modelo.FechaNacimiento = dato.FechaNacimiento;
                modelo.Estado = dato.Estado;
                modelo.Provincia = Convert.ToChar(dato.Provincia);
                modelo.Canton = dato.Canton;
                modelo.Distrito = dato.Distrito;
                return View(modelo);
            }
            catch (Exception)
            {

                throw;
            }

        }
    [Authorize(Roles = "Administrador")]
    [HttpPost]
        public ActionResult Editar(int id, Usuario usuario)
        {
            try
            {

                var SecretKey = ConfigurationManager.AppSettings["SecretKey"];
                clsUsuario ObjUsuario = new clsUsuario();
                bool Resultado = ObjUsuario.ActualizarUsuario(id, usuario.IdInstitucion, usuario.IdTipoIdentificacion, usuario.Identificacion,
                   usuario.Nombre, usuario.Apellido1, usuario.Apellido2, Seguridad.EncryptString(SecretKey, usuario.Password), usuario.Telefono, usuario.Direccion,
                   usuario.Correo, usuario.FechaNacimiento, usuario.Estado, usuario.Provincia, usuario.Canton, usuario.Distrito);

                if (Resultado)
                {
          int us = Convert.ToInt32(Session["IdUsuario"].ToString());
          Objbitacora.AgregarBitacora("UsuarioController", "Editar", "Exito", 1, DateTime.UtcNow.Date, us);

          return RedirectToAction("Index");
                }
                else
                {
          ViewBag.ListaInstitucion = CargarInstitucion();
          ViewBag.ListaTipoIdentificacion = CargarTipoIdentificacion();
          ViewBag.ListaProvincias = CargaProvincias();
          ViewBag.ListaEstado = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");


          return View("Editar");
                }
            }
            catch
            {
        int us = Convert.ToInt32(Session["IdUsuario"].ToString());
        Objbitacora.AgregarBitacora("UsuarioController", "Editar", "Error en la edicion de los datos", 1, DateTime.UtcNow.Date, us);

        return View();
            }
        }
    [Authorize(Roles = "Administrador")]
    public ActionResult Eliminar(int id)
        {
            try
            {
                clsUsuario objcliente = new clsUsuario();
                bool Resultado = objcliente.EliminarUsuario(id);

                if (Resultado)
                {
          int us = Convert.ToInt32(Session["IdUsuario"].ToString());
          Objbitacora.AgregarBitacora("UsuarioController", "Eliminar", "Exito", 1, DateTime.UtcNow.Date, us);

          return RedirectToAction("Index");
                }
                else
                {
                    return RedirectToAction("Index");
                }

            }
            catch
            {
        int us = Convert.ToInt32(Session["IdUsuario"].ToString());
        Objbitacora.AgregarBitacora("UsuarioController", "Eliminar", "Error en la eliminacion de los datos", 1, DateTime.UtcNow.Date, us);

        return View();
            }
        }
        clsInstitucion Institucion = new clsInstitucion();
        public List<ConsultarInstitucionResult> CargarInstitucion()
        {
            List<ConsultarInstitucionResult> instituciones = Institucion.ConsultarInstitucion();
            return instituciones;
        }
        clsTipoIdentificacion TipoIdentificacion = new clsTipoIdentificacion();
        public List<ConsultarTipoIdentificacionResult> CargarTipoIdentificacion()
        {
            List<ConsultarTipoIdentificacionResult> tipoIdentificacion = TipoIdentificacion.ConsultarTipoIdentificacion();
            return tipoIdentificacion;
        }
        clsInformacion Informacion = new clsInformacion();
        /// <summary>
        /// Obtiene Provincias
        /// </summary>
        /// <returns></returns>
        public List<ProvinciasResult> CargaProvincias()
        {
            List<ProvinciasResult> provincias = Informacion.ObtenerProvincias();
            return provincias;
        }
        /// <summary>
        /// Obtiene Cantones
        /// </summary>
        /// <param name="provincia"></param>
        /// <returns></returns>
        public List<CantonesResult> CargaCanton(char provincia)
        {
            List<CantonesResult> cantones = Informacion.ObtenerCantones(provincia);
            return cantones;
        }
        /// <summary>
        /// Obtiene Distritos
        /// </summary>
        /// <param name="provincia"></param>
        /// <param name="canton"></param>
        /// <returns></returns>
        public List<DistritosResult> CargaDistrito(char provincia, string canton)
        {
            List<DistritosResult> distritos = Informacion.ObtenerDistritos(provincia, canton);
            return distritos;
        }
        /// <summary>
        /// Cargar Cantones hacia la pantalla
        /// </summary>
        /// <param name="provincia"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult CargaCantones(char provincia)
        {
            List<CantonesResult> cantones = Informacion.ObtenerCantones(provincia);
            return Json(cantones, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Cargar Disttritos hacia la pantalla
        /// </summary>
        /// <param name="provincia"></param>
        /// <param name="canton"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult CargaDistritos(char provincia, string canton)
        {
            List<DistritosResult> distritos = Informacion.ObtenerDistritos(provincia, canton);
            return Json(distritos, JsonRequestBehavior.AllowGet);
        }
    }
}
