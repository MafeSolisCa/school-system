using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAL;
using BLL;
using Proyecto.Models;

namespace Proyecto.Controllers
{
  public class TipoIdentificacionController : Controller
  {
    clsBitacora Objbitacora = new clsBitacora();
    [Authorize(Roles = "Administrador")]
      public ActionResult Index()
      {
        try
        {
          List<TipoIdentificacion> listaTipoIdentificacion = new List<TipoIdentificacion>();
          clsTipoIdentificacion TipoIdentificacion = new clsTipoIdentificacion();
          var data = TipoIdentificacion.ConsultarTipoIdentificacion().ToList();

          foreach (var item in data)
          {
            TipoIdentificacion modelo = new TipoIdentificacion();
            modelo.IdTipoIdentificacion = item.IdTipoIdentificacion;
            modelo.Descripcion = item.Descripcion;
            modelo.Estado = item.Estado;
            listaTipoIdentificacion.Add(modelo);
          }
          return View(listaTipoIdentificacion);
        }
        catch
        {
        int us = Convert.ToInt32(Session["IdUsuario"].ToString());
        Objbitacora.AgregarBitacora("TipoIdentificacionController", "Index", "Eror en la consulta de los datos", 1, DateTime.UtcNow.Date, us);

        return RedirectToAction("Error", "Error");
        }
      }
      [Authorize(Roles = "Administrador")]
      public ActionResult Crear()
      {
        try
        {
          ViewBag.ListaEstados = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");
          return View();
        }
        catch (Exception)
        {

          throw;
        }

      }


      [Authorize(Roles = "Administrador")]
      [HttpPost]
      public ActionResult Crear(TipoIdentificacion tipoIdentificacion)
      {
        try
        {
          if (ModelState.IsValid)
          {

            clsTipoIdentificacion TipoIdentificacion = new clsTipoIdentificacion();
            bool Resultado = TipoIdentificacion.AgregarTipoIdentificacion(tipoIdentificacion.Descripcion, tipoIdentificacion.Estado);
            clsBitacora Objbitacora = new clsBitacora();

            if (Resultado)
            {
            int us = Convert.ToInt32(Session["IdUsuario"].ToString());
            Objbitacora.AgregarBitacora("TipoIdentificacionController", "Crear", "Exito", 1, DateTime.UtcNow.Date, us);

            return RedirectToAction("Index");
            }
            else
            {
              ViewBag.ListaEstados = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");
              return View("Crear");
            }
          }
          else
          {
            ViewBag.ListaEstados = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");
            return View("Crear");
          }


        }
        catch
        {
        int us = Convert.ToInt32(Session["IdUsuario"].ToString());
        Objbitacora.AgregarBitacora("TipoIdentificacionController", "Crear", "Error en creacion de los datos", 1, DateTime.UtcNow.Date, us);

        return View();
        }
      }
      [Authorize(Roles = "Administrador")]
      public ActionResult Editar(int id)
      {
        try
        {
          clsTipoIdentificacion TipoIdentificacion = new clsTipoIdentificacion();
        ViewBag.ListaEstados = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");
        var dato = TipoIdentificacion.ConsultaTipoIdentificacion(id);

          TipoIdentificacion modelo = new TipoIdentificacion();
          modelo.IdTipoIdentificacion = dato.IdTipoIdentificacion;
          modelo.Descripcion = dato.Descripcion;
          modelo.Estado = dato.Estado;
          return View(modelo);
        }
        catch (Exception)
        {

          throw;
        }

      }
      [Authorize(Roles = "Administrador")]
      [HttpPost]
      public ActionResult Editar(int id, TipoIdentificacion tipoIdentificacion)
      {
        try
        {
          clsTipoIdentificacion TipoIdentificacion = new clsTipoIdentificacion();
          bool Resultado = TipoIdentificacion.ActualizarTipoIdentificacion(id, tipoIdentificacion.Descripcion, tipoIdentificacion.Estado);

          if (Resultado)
          {
          int us = Convert.ToInt32(Session["IdUsuario"].ToString());
          Objbitacora.AgregarBitacora("TipoIdentificacionController", "Editar", "Exito", 1, DateTime.UtcNow.Date, us);

          return RedirectToAction("Index");
          }
          else
          {
            ViewBag.ListaEstados = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");
            return View("Editar");
          }
        }
        catch
        {
        int us = Convert.ToInt32(Session["IdUsuario"].ToString());
        Objbitacora.AgregarBitacora("TipoIdentificacionController", "Editar", "Error en la edicion de los datos", 1, DateTime.UtcNow.Date, us);

        return View();
        }
      }
      [Authorize(Roles = "Administrador")]
      public ActionResult Eliminar(int id)
      {
        try
        {
          clsTipoIdentificacion TipoIdentificacion = new clsTipoIdentificacion();
          bool Resultado = TipoIdentificacion.EliminarTipoIdentificacion(id);

          if (Resultado)
          {
          int us = Convert.ToInt32(Session["IdUsuario"].ToString());
          Objbitacora.AgregarBitacora("TipoIdentificacionController", "Eliminar", "Exito", 1, DateTime.UtcNow.Date, us);

          return RedirectToAction("Index");
          }
          else
          {
            return RedirectToAction("Index");
          }

        }
        catch
        {
        int us = Convert.ToInt32(Session["IdUsuario"].ToString());
        Objbitacora.AgregarBitacora("TipoIdentificacionController", "Eliminar", "Error en la eliminacion de los datos", 1, DateTime.UtcNow.Date, us);

        return View();
        }
      }
    }
  }

