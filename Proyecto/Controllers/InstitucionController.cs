using BLL;
using DAL;
using Proyecto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Proyecto.Controllers
{
    public class InstitucionController : Controller
    {
    clsBitacora Objbitacora = new clsBitacora();
    [Authorize(Roles = "Administrador")]
    public ActionResult Index()
    {
      try
      {
        List<Institucion> listaRol = new List<Institucion>();
        clsInstitucion Institucion = new clsInstitucion();
        var data = Institucion.ConsultarInstitucion().ToList();

        foreach (var item in data)
        {
          Institucion modelo = new Institucion();
          modelo.IdInstitucion = item.IdInstitucion;
          modelo.Descripcion = item.Descripcion;
          modelo.Telefono = item.Telefono;
          modelo.Encargado = item.Encargado;
          modelo.Direccion = item.Direccion;
          modelo.Provincia = item.Provincia;
          modelo.Canton = item.Canton;
          modelo.Distrito = item.Distrito;
          modelo.Estado = item.Estado;
          listaRol.Add(modelo);
        }
        return View(listaRol);
      }
      catch
      {
        int us = Convert.ToInt32(Session["IdUsuario"].ToString());
        Objbitacora.AgregarBitacora("InstitucionController", "Index", "Eror en la consulta de los datos", 1, DateTime.UtcNow.Date, us);

        return RedirectToAction("Error", "Error");
      }
    }
    [Authorize(Roles = "Administrador")]
    public ActionResult Crear()
    {
      try
      {
        ViewBag.ListaProvincias = CargaProvincias();
        ViewBag.ListaEstados = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");
        return View();
      }
      catch (Exception)
      {

        throw;
      }

    }

    [Authorize(Roles = "Administrador")]
    [HttpPost]
    public ActionResult Crear(Institucion institucion)
    {
      try
      {
        if (ModelState.IsValid)
        {

          clsInstitucion Institucion = new clsInstitucion();
          bool Resultado = Institucion.AgregarInstitucion(institucion.Descripcion, institucion.Telefono, institucion.Encargado,
            institucion.Direccion, institucion.Provincia, institucion.Canton, institucion.Distrito, institucion.Estado);
          clsBitacora Objbitacora = new clsBitacora();

          if (Resultado)
          {
            int us = Convert.ToInt32(Session["IdUsuario"].ToString());
            Objbitacora.AgregarBitacora("InstitucionController", "Crear", "Exito", 1, DateTime.UtcNow.Date, us);

            return RedirectToAction("Index");
          }
          else
          {
            ViewBag.ListaProvincias = CargaProvincias();
            ViewBag.ListaEstados = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");
            return View("Crear");
          }
        }
        else
        {
          ViewBag.ListaEstados = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");
          return View("Crear");
        }

      }
      catch
      {
        int us = Convert.ToInt32(Session["IdUsuario"].ToString());
        Objbitacora.AgregarBitacora("InstitucionController", "Crear", "Error en creacion de los datos", 1, DateTime.UtcNow.Date, us);

        return View();
      }
    }
    [Authorize(Roles = "Administrador")]
    public ActionResult Editar(int id)
    {
      try
      {
        clsInstitucion Institucion = new clsInstitucion();
        ViewBag.ListaProvincias = CargaProvincias();
        ViewBag.ListaEstados = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");
        var dato = Institucion.ConsultaInstitucion(id);

        Institucion modelo = new Institucion();
        modelo.IdInstitucion = dato.IdInstitucion;
        modelo.Descripcion = dato.Descripcion;
        modelo.Telefono = dato.Telefono;
        modelo.Encargado = dato.Encargado;
        modelo.Direccion = dato.Direccion;
        modelo.Provincia = dato.Provincia;
        modelo.Canton = dato.Canton;
        modelo.Distrito = dato.Distrito;
        modelo.Estado = dato.Estado;
        return View(modelo);
      }
      catch (Exception)
      {

        throw;
      }

    }
    [Authorize(Roles = "Administrador")]
    [HttpPost]
    public ActionResult Editar(int id, Institucion institucion)
    {
      try
      {
        clsInstitucion Institucion = new clsInstitucion();
        bool Resultado = Institucion.ActualizarInstitucion(id, institucion.Descripcion, institucion.Telefono, institucion.Encargado, institucion.Direccion, institucion.Provincia, institucion.Canton, institucion.Distrito, institucion.Estado);

        if (Resultado)
        {
          int us = Convert.ToInt32(Session["IdUsuario"].ToString());
          Objbitacora.AgregarBitacora("InstitucionController", "Editar", "Exito", 1, DateTime.UtcNow.Date, us);

          return RedirectToAction("Index");
        }
        else
        {
          ViewBag.ListaProvincias = CargaProvincias();
          ViewBag.ListaEstados = new SelectList(new[] {
                                   new SelectListItem { Value = "true", Text = "Activo" },
                                   new SelectListItem { Value = "false", Text = "Inactivo" }
                                                               }, "Value", "Text");
          return View("Editar");
        }
      }
      catch
      {
        int us = Convert.ToInt32(Session["IdUsuario"].ToString());
        Objbitacora.AgregarBitacora("InstitucionController", "Editar", "Error en la edicion de los datos", 1, DateTime.UtcNow.Date, us);

        return View();
      }
    }
    [Authorize(Roles = "Administrador")]
    public ActionResult Eliminar(int id)
    {
      try
      {
        clsInstitucion Institucion = new clsInstitucion();
        bool Resultado = Institucion.EliminarInstitucion(id);

        if (Resultado)
        {
          int us = Convert.ToInt32(Session["IdUsuario"].ToString());
          Objbitacora.AgregarBitacora("InstitucionController", "Eliminar", "Exito", 1, DateTime.UtcNow.Date, us);

          return RedirectToAction("Index");
        }
        else
        {
          return RedirectToAction("Index");
        }

      }
      catch
      {
        int us = Convert.ToInt32(Session["IdUsuario"].ToString());
        Objbitacora.AgregarBitacora("InstitucionController", "Eliminar", "Error en la eliminacion de los datos", 1, DateTime.UtcNow.Date, us);

        return View();
      }
    }
    clsInformacion Informacion = new clsInformacion();
    /// <summary>
    /// Obtiene Provincias
    /// </summary>
    /// <returns></returns>
    public List<ProvinciasResult> CargaProvincias()
    {
      List<ProvinciasResult> provincias = Informacion.ObtenerProvincias();
      return provincias;
    }
    /// <summary>
    /// Obtiene Cantones
    /// </summary>
    /// <param name="provincia"></param>
    /// <returns></returns>
    public List<CantonesResult> CargaCanton(char provincia)
    {
      List<CantonesResult> cantones = Informacion.ObtenerCantones(provincia);
      return cantones;
    }
    /// <summary>
    /// Obtiene Distritos
    /// </summary>
    /// <param name="provincia"></param>
    /// <param name="canton"></param>
    /// <returns></returns>
    public List<DistritosResult> CargaDistrito(char provincia, string canton)
    {
      List<DistritosResult> distritos = Informacion.ObtenerDistritos(provincia, canton);
      return distritos;
    }
    /// <summary>
    /// Cargar Cantones hacia la pantalla
    /// </summary>
    /// <param name="provincia"></param>
    /// <returns></returns>
    [HttpPost]
    public JsonResult CargaCantones(char provincia)
    {
      List<CantonesResult> cantones = Informacion.ObtenerCantones(provincia);
      return Json(cantones, JsonRequestBehavior.AllowGet);
    }
    /// <summary>
    /// Cargar Disttritos hacia la pantalla
    /// </summary>
    /// <param name="provincia"></param>
    /// <param name="canton"></param>
    /// <returns></returns>
    [HttpPost]
    public JsonResult CargaDistritos(char provincia, string canton)
    {
      List<DistritosResult> distritos = Informacion.ObtenerDistritos(provincia, canton);
      return Json(distritos, JsonRequestBehavior.AllowGet);
    }
  }
}
