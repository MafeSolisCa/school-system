using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto.Models
{
  public class NotaList
  {
    public int IdNota { get; set; }
    public string Identificacion { get; set; }
    public string Nombre { get; set; }
    public string Materia { get; set; }
    public decimal Nota { get; set; }
    public bool Estado { get; set; }
  }
}
