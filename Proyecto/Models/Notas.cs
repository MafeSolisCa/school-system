using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto.Models
{
  public class Notas
  {
    public int IdNota { get; set; }
    public int IdEstudiante { get; set; }
    public int IdMateria { get; set; }
    public decimal Nota { get; set; }
    public bool Estado { get; set; }
  }
}
