using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto.Models
{
  public class DocenteGradoList
  {
    public int IdDocenteGrado { get; set; }
    public string NombreDocente { get; set; }
    public string Grado { get; set; }
  }
}
