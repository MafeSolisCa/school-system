﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto.Models
{
    public class DocenteList
    {
        public int IdDocente { get; set; }
        public string InstitucionNombre { get; set; }
        public string Nombre { get; set; }
        public string MateriaNombre { get; set; }
        public bool Estado { get; set; }
    }
}