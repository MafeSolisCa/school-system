using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto.Models
{
  public class CalificacionList
  {
    public int IdCalificacion { get; set; }
    public string TipoOperacion { get; set; }
    public string NombreEstudiante { get; set; }
    public string NombreDocente { get; set; }
    public string Materia { get; set; }
    public decimal Nota { get; set; }
    public DateTime Fecha { get; set; }
  }
}
