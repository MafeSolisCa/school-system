﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Proyecto.Models
{
    public class EstudianteList
    {
        public int IdEstudiante { get; set; }
        public string Institucion { get; set; }
        public string Cedula { get; set; }
        public string Nombre { get; set; }
        public string Grado { get; set; }
        public bool Estado { get; set; }
    }
}