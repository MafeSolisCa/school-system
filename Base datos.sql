USE [DBAvanzada]
GO
/****** Object:  Table [dbo].[Bitacora]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bitacora](
	[IdBitacora] [int] NOT NULL,
	[Controlador] [varchar](50) NOT NULL,
	[Accion] [varchar](50) NOT NULL,
	[Error] [varchar](max) NULL,
	[Tipo] [int] NOT NULL,
	[Fecha] [datetime] NOT NULL,
	[IdUsuario] [int] NOT NULL,
 CONSTRAINT [PK_Bitacora] PRIMARY KEY CLUSTERED 
(
	[IdBitacora] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Calendario]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Calendario](
	[IdEvento] [int] NOT NULL,
	[Asunto] [varchar](100) NOT NULL,
	[Descripcion] [varchar](500) NOT NULL,
	[Inicia] [datetime] NOT NULL,
	[Finaliza] [datetime] NOT NULL,
	[TodoElDia] [bit] NOT NULL,
	[Color] [varchar](10) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdEvento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Calificacion]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Calificacion](
	[IdCalificacion] [int] NOT NULL,
	[IdTipoOperacion] [int] NOT NULL,
	[IdEstudiante] [int] NOT NULL,
	[IdDocente] [int] NOT NULL,
	[IdMateria] [int] NOT NULL,
	[Nota] [decimal](4, 2) NOT NULL,
	[Fecha] [datetime] NOT NULL,
 CONSTRAINT [PK_Calificacion] PRIMARY KEY CLUSTERED 
(
	[IdCalificacion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Canton]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Canton](
	[Provincia] [char](1) NOT NULL,
	[Canton] [char](2) NOT NULL,
	[Nombre] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[Provincia] ASC,
	[Canton] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Distrito]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Distrito](
	[Provincia] [char](1) NOT NULL,
	[Canton] [char](2) NOT NULL,
	[Distrito] [char](2) NOT NULL,
	[Nombre] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[Provincia] ASC,
	[Canton] ASC,
	[Distrito] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Docente]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Docente](
	[IdDocente] [int] NOT NULL,
	[IdInstitucion] [int] NOT NULL,
	[IdUsuario] [int] NOT NULL,
	[IdMateria] [int] NULL,
	[Estado] [bit] NOT NULL,
 CONSTRAINT [PK_Docente] PRIMARY KEY CLUSTERED 
(
	[IdDocente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DocenteGrado]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DocenteGrado](
	[IdDocenteGrado] [int] NOT NULL,
	[IdDocente] [int] NOT NULL,
	[IdGrado] [int] NOT NULL,
 CONSTRAINT [PK_DocenteGrado] PRIMARY KEY CLUSTERED 
(
	[IdDocenteGrado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Estudiante]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Estudiante](
	[IdEstudiante] [int] NOT NULL,
	[IdInstitucion] [int] NOT NULL,
	[IdUsuario] [int] NOT NULL,
	[IdGrado] [int] NULL,
	[Estado] [bit] NOT NULL,
 CONSTRAINT [PK_Estudiante] PRIMARY KEY CLUSTERED 
(
	[IdEstudiante] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Grado]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Grado](
	[IdGrado] [int] NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[Estado] [bit] NOT NULL,
 CONSTRAINT [PK_Grado] PRIMARY KEY CLUSTERED 
(
	[IdGrado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Horario]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Horario](
	[IdHorario] [int] NOT NULL,
	[IdGrado] [int] NOT NULL,
	[Materia] [nvarchar](100) NOT NULL,
	[Descripcion] [nvarchar](200) NOT NULL,
	[Inicio] [datetime] NOT NULL,
	[Fin] [datetime] NOT NULL,
	[ColorFondo] [nvarchar](10) NOT NULL,
	[DiaCompleto] [bit] NOT NULL,
 CONSTRAINT [PK_Horario] PRIMARY KEY CLUSTERED 
(
	[IdHorario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Institucion]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Institucion](
	[IdInstitucion] [int] NOT NULL,
	[Descripcion] [varchar](200) NOT NULL,
	[Telefono] [varchar](50) NOT NULL,
	[Encargado] [varchar](50) NOT NULL,
	[Direccion] [varchar](500) NULL,
	[Provincia] [char](1) NOT NULL,
	[Canton] [char](2) NOT NULL,
	[Distrito] [char](2) NOT NULL,
	[Estado] [bit] NOT NULL,
 CONSTRAINT [PK_Institucion] PRIMARY KEY CLUSTERED 
(
	[IdInstitucion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Materia]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Materia](
	[IdMateria] [int] NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[IdInstitucion] [int] NULL,
	[Estado] [bit] NOT NULL,
 CONSTRAINT [PK_Materia] PRIMARY KEY CLUSTERED 
(
	[IdMateria] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MateriaGrado]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MateriaGrado](
	[IdMateriaGrado] [int] NOT NULL,
	[IdMateria] [int] NOT NULL,
	[IdGrado] [int] NOT NULL,
 CONSTRAINT [PK_MateriaGrado] PRIMARY KEY CLUSTERED 
(
	[IdMateriaGrado] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Nota]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Nota](
	[IdNota] [int] NOT NULL,
	[IdEstudiante] [int] NOT NULL,
	[IdMateria] [int] NOT NULL,
	[Nota] [decimal](4, 2) NOT NULL,
	[Estado] [bit] NOT NULL,
 CONSTRAINT [PK_Nota] PRIMARY KEY CLUSTERED 
(
	[IdNota] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Provincia]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Provincia](
	[Provincia] [char](1) NOT NULL,
	[Nombre] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[Provincia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Nombre] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Rol]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Rol](
	[IdRol] [int] NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[Estado] [bit] NOT NULL,
 CONSTRAINT [PK_ROL] PRIMARY KEY CLUSTERED 
(
	[IdRol] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TipoIdentificacion]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoIdentificacion](
	[IdTipoIdentificacion] [int] NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[Estado] [bit] NOT NULL,
 CONSTRAINT [PK_TipoIdentificacion] PRIMARY KEY CLUSTERED 
(
	[IdTipoIdentificacion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TipoOperacion]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoOperacion](
	[IdTipoOperacion] [int] NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[Estado] [bit] NOT NULL,
 CONSTRAINT [PK_TipoOperacion] PRIMARY KEY CLUSTERED 
(
	[IdTipoOperacion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Usuario]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuario](
	[IdUsuario] [int] NOT NULL,
	[IdInstitucion] [int] NOT NULL,
	[IdTipoIdentificacion] [int] NOT NULL,
	[Identificacion] [varchar](50) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Apellido1] [varchar](50) NOT NULL,
	[Apellido2] [varchar](50) NOT NULL,
	[Password] [varchar](50) NOT NULL,
	[Telefono] [varchar](50) NULL,
	[Direccion] [varchar](500) NULL,
	[Correo] [varchar](100) NULL,
	[FechaNacimiento] [datetime] NOT NULL,
	[Estado] [bit] NOT NULL,
	[Provincia] [char](1) NULL,
	[Canton] [char](2) NULL,
	[Distrito] [char](2) NULL,
 CONSTRAINT [PK_Usuario] PRIMARY KEY CLUSTERED 
(
	[IdUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UsuarioRol]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsuarioRol](
	[Id] [int] NOT NULL,
	[IdUsuario] [int] NOT NULL,
	[IdRol] [int] NOT NULL,
 CONSTRAINT [PK_UsuarioRol] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Bitacora] ADD  CONSTRAINT [DF_Bitacora_Fecha]  DEFAULT (getdate()) FOR [Fecha]
GO
ALTER TABLE [dbo].[Calificacion]  WITH CHECK ADD  CONSTRAINT [FK_Calificacion_Docente] FOREIGN KEY([IdDocente])
REFERENCES [dbo].[Docente] ([IdDocente])
GO
ALTER TABLE [dbo].[Calificacion] CHECK CONSTRAINT [FK_Calificacion_Docente]
GO
ALTER TABLE [dbo].[Calificacion]  WITH CHECK ADD  CONSTRAINT [FK_Calificacion_Estudiante] FOREIGN KEY([IdEstudiante])
REFERENCES [dbo].[Estudiante] ([IdEstudiante])
GO
ALTER TABLE [dbo].[Calificacion] CHECK CONSTRAINT [FK_Calificacion_Estudiante]
GO
ALTER TABLE [dbo].[Calificacion]  WITH CHECK ADD  CONSTRAINT [FK_Calificacion_Materia] FOREIGN KEY([IdMateria])
REFERENCES [dbo].[Materia] ([IdMateria])
GO
ALTER TABLE [dbo].[Calificacion] CHECK CONSTRAINT [FK_Calificacion_Materia]
GO
ALTER TABLE [dbo].[Calificacion]  WITH CHECK ADD  CONSTRAINT [FK_Calificacion_TipoOperacion] FOREIGN KEY([IdTipoOperacion])
REFERENCES [dbo].[TipoOperacion] ([IdTipoOperacion])
GO
ALTER TABLE [dbo].[Calificacion] CHECK CONSTRAINT [FK_Calificacion_TipoOperacion]
GO
ALTER TABLE [dbo].[Canton]  WITH CHECK ADD FOREIGN KEY([Provincia])
REFERENCES [dbo].[Provincia] ([Provincia])
GO
ALTER TABLE [dbo].[Canton]  WITH CHECK ADD FOREIGN KEY([Provincia])
REFERENCES [dbo].[Provincia] ([Provincia])
GO
ALTER TABLE [dbo].[Distrito]  WITH CHECK ADD FOREIGN KEY([Provincia], [Canton])
REFERENCES [dbo].[Canton] ([Provincia], [Canton])
GO
ALTER TABLE [dbo].[Docente]  WITH CHECK ADD  CONSTRAINT [FK_Docente_Institucion] FOREIGN KEY([IdInstitucion])
REFERENCES [dbo].[Institucion] ([IdInstitucion])
GO
ALTER TABLE [dbo].[Docente] CHECK CONSTRAINT [FK_Docente_Institucion]
GO
ALTER TABLE [dbo].[Docente]  WITH CHECK ADD  CONSTRAINT [FK_Docente_Materia] FOREIGN KEY([IdMateria])
REFERENCES [dbo].[Materia] ([IdMateria])
GO
ALTER TABLE [dbo].[Docente] CHECK CONSTRAINT [FK_Docente_Materia]
GO
ALTER TABLE [dbo].[Docente]  WITH CHECK ADD  CONSTRAINT [FK_Docente_Usuario] FOREIGN KEY([IdUsuario])
REFERENCES [dbo].[Usuario] ([IdUsuario])
GO
ALTER TABLE [dbo].[Docente] CHECK CONSTRAINT [FK_Docente_Usuario]
GO
ALTER TABLE [dbo].[DocenteGrado]  WITH CHECK ADD  CONSTRAINT [FK_DocenteGrado_Docente] FOREIGN KEY([IdDocente])
REFERENCES [dbo].[Docente] ([IdDocente])
GO
ALTER TABLE [dbo].[DocenteGrado] CHECK CONSTRAINT [FK_DocenteGrado_Docente]
GO
ALTER TABLE [dbo].[DocenteGrado]  WITH CHECK ADD  CONSTRAINT [FK_DocenteGrado_Grado] FOREIGN KEY([IdGrado])
REFERENCES [dbo].[Grado] ([IdGrado])
GO
ALTER TABLE [dbo].[DocenteGrado] CHECK CONSTRAINT [FK_DocenteGrado_Grado]
GO
ALTER TABLE [dbo].[Estudiante]  WITH CHECK ADD  CONSTRAINT [FK_Estudiante_Grado] FOREIGN KEY([IdGrado])
REFERENCES [dbo].[Grado] ([IdGrado])
GO
ALTER TABLE [dbo].[Estudiante] CHECK CONSTRAINT [FK_Estudiante_Grado]
GO
ALTER TABLE [dbo].[Estudiante]  WITH CHECK ADD  CONSTRAINT [FK_Estudiante_Grado1] FOREIGN KEY([IdGrado])
REFERENCES [dbo].[Grado] ([IdGrado])
GO
ALTER TABLE [dbo].[Estudiante] CHECK CONSTRAINT [FK_Estudiante_Grado1]
GO
ALTER TABLE [dbo].[Estudiante]  WITH CHECK ADD  CONSTRAINT [FK_Estudiante_Institucion1] FOREIGN KEY([IdInstitucion])
REFERENCES [dbo].[Institucion] ([IdInstitucion])
GO
ALTER TABLE [dbo].[Estudiante] CHECK CONSTRAINT [FK_Estudiante_Institucion1]
GO
ALTER TABLE [dbo].[Estudiante]  WITH CHECK ADD  CONSTRAINT [FK_Estudiante_Usuario] FOREIGN KEY([IdUsuario])
REFERENCES [dbo].[Usuario] ([IdUsuario])
GO
ALTER TABLE [dbo].[Estudiante] CHECK CONSTRAINT [FK_Estudiante_Usuario]
GO
ALTER TABLE [dbo].[Horario]  WITH CHECK ADD  CONSTRAINT [FK_Horario_Grado] FOREIGN KEY([IdGrado])
REFERENCES [dbo].[Grado] ([IdGrado])
GO
ALTER TABLE [dbo].[Horario] CHECK CONSTRAINT [FK_Horario_Grado]
GO
ALTER TABLE [dbo].[Materia]  WITH CHECK ADD  CONSTRAINT [FK_Materia_Institucion] FOREIGN KEY([IdInstitucion])
REFERENCES [dbo].[Institucion] ([IdInstitucion])
GO
ALTER TABLE [dbo].[Materia] CHECK CONSTRAINT [FK_Materia_Institucion]
GO
ALTER TABLE [dbo].[MateriaGrado]  WITH CHECK ADD  CONSTRAINT [FK_MateriaGrado_Grado] FOREIGN KEY([IdGrado])
REFERENCES [dbo].[Grado] ([IdGrado])
GO
ALTER TABLE [dbo].[MateriaGrado] CHECK CONSTRAINT [FK_MateriaGrado_Grado]
GO
ALTER TABLE [dbo].[MateriaGrado]  WITH CHECK ADD  CONSTRAINT [FK_MateriaGrado_Materia] FOREIGN KEY([IdMateria])
REFERENCES [dbo].[Materia] ([IdMateria])
GO
ALTER TABLE [dbo].[MateriaGrado] CHECK CONSTRAINT [FK_MateriaGrado_Materia]
GO
ALTER TABLE [dbo].[Nota]  WITH CHECK ADD  CONSTRAINT [FK_Nota_Estudiante] FOREIGN KEY([IdEstudiante])
REFERENCES [dbo].[Estudiante] ([IdEstudiante])
GO
ALTER TABLE [dbo].[Nota] CHECK CONSTRAINT [FK_Nota_Estudiante]
GO
ALTER TABLE [dbo].[Nota]  WITH CHECK ADD  CONSTRAINT [FK_Nota_Materia] FOREIGN KEY([IdMateria])
REFERENCES [dbo].[Materia] ([IdMateria])
GO
ALTER TABLE [dbo].[Nota] CHECK CONSTRAINT [FK_Nota_Materia]
GO
ALTER TABLE [dbo].[Usuario]  WITH CHECK ADD  CONSTRAINT [FK_Usuario_Institucion] FOREIGN KEY([IdInstitucion])
REFERENCES [dbo].[Institucion] ([IdInstitucion])
GO
ALTER TABLE [dbo].[Usuario] CHECK CONSTRAINT [FK_Usuario_Institucion]
GO
ALTER TABLE [dbo].[Usuario]  WITH CHECK ADD  CONSTRAINT [FK_Usuario_TipoIdentificacion] FOREIGN KEY([IdTipoIdentificacion])
REFERENCES [dbo].[TipoIdentificacion] ([IdTipoIdentificacion])
GO
ALTER TABLE [dbo].[Usuario] CHECK CONSTRAINT [FK_Usuario_TipoIdentificacion]
GO
ALTER TABLE [dbo].[UsuarioRol]  WITH CHECK ADD  CONSTRAINT [FK_UsuarioRol_ROL] FOREIGN KEY([IdRol])
REFERENCES [dbo].[Rol] ([IdRol])
GO
ALTER TABLE [dbo].[UsuarioRol] CHECK CONSTRAINT [FK_UsuarioRol_ROL]
GO
ALTER TABLE [dbo].[UsuarioRol]  WITH CHECK ADD  CONSTRAINT [FK_UsuarioRol_Usuario] FOREIGN KEY([IdUsuario])
REFERENCES [dbo].[Usuario] ([IdUsuario])
GO
ALTER TABLE [dbo].[UsuarioRol] CHECK CONSTRAINT [FK_UsuarioRol_Usuario]
GO
/****** Object:  StoredProcedure [dbo].[ActualizarBitacora]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[ActualizarBitacora]
@IdBitacora int,
@Controlador varchar(50),
@Accion varchar(50),
@Error varchar(max),
@Tipo int,
@Fecha datetime,
@IdUsuario int
AS
BEGIN

    Update Bitacora set           
           Controlador=@Controlador
           ,Accion=@Accion
           ,Error=@Error
           ,Tipo=@Tipo
           ,Fecha=@Fecha
           ,IdUsuario=@IdUsuario
		where IdBitacora=@IdBitacora
	
END
GO
/****** Object:  StoredProcedure [dbo].[ActualizarCalendario]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[ActualizarCalendario]
@IdEvento int,
@Asunto varchar(100),
@Descripcion varchar(500),
@Inicia datetime,
@Finaliza datetime,
@TodoElDia bit,
@Color varchar(10)
AS
BEGIN
    Update Calendario set
            Asunto=@Asunto
           ,Descripcion=@Descripcion
           ,Inicia=@Inicia
           ,Finaliza=@Finaliza
		   ,TodoElDia=@TodoElDia
           ,Color=@Color
		where IdEvento=@IdEvento
	
END
GO
/****** Object:  StoredProcedure [dbo].[ActualizarCalificacion]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[ActualizarCalificacion]
@IdCalificacion int,
@IdTipoOperacion int,
@IdEstudiante int,
@IdDocente int,
@IdMateria int,
@Nota decimal(4,2),
@Fecha datetime
AS
BEGIN

    Update Calificacion set           
           IdTipoOperacion=@IdTipoOperacion
           ,IdEstudiante=@IdEstudiante
           ,IdDocente=@IdDocente
           ,IdMateria=@IdMateria
           ,Nota=@Nota
           ,Fecha=@Fecha
		where IdCalificacion=@IdCalificacion
	
END
GO
/****** Object:  StoredProcedure [dbo].[ActualizarDocente]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[ActualizarDocente]
@IdDocente int,
@IdInstitucion int,
@IdUsuario int,
@IdMateria int,
@Estado bit
AS
BEGIN

    Update Docente set           
           IdInstitucion=@IdInstitucion
           ,IdUsuario=@IdUsuario
		   ,IdMateria=@IdMateria
		   ,Estado=@Estado
		where IdDocente=@IdDocente
	
END
GO
/****** Object:  StoredProcedure [dbo].[ActualizarDocenteGrado]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[ActualizarDocenteGrado]
@IdDocenteGrado int,
@IdDocente int,
@IdGrado int
AS
BEGIN

    Update DocenteGrado set           
           IdDocente=@IdDocente
           ,IdGrado=@IdGrado
		where IdDocenteGrado=@IdDocenteGrado
	
END
GO
/****** Object:  StoredProcedure [dbo].[ActualizarEstudiante]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[ActualizarEstudiante]
@IdEstudiante int,
@IdInstitucion int,
@IdUsuario int,
@IdGrado int,
@Estado bit
AS
BEGIN

    Update Estudiante set           
           IdInstitucion=@IdInstitucion
           ,IdUsuario=@IdUsuario
		   ,IdGrado=@IdGrado
		   ,Estado=@Estado
		where IdEstudiante=@IdEstudiante
	
END
GO
/****** Object:  StoredProcedure [dbo].[ActualizarGrado]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[ActualizarGrado]
@IdGrado int,
@Descripcion varchar(50),
@Estado bit
AS
BEGIN

    Update Grado set           
           Descripcion=@Descripcion
           ,Estado=@Estado
		where IdGrado=@IdGrado
	
END
GO
/****** Object:  StoredProcedure [dbo].[ActualizarHorario]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[ActualizarHorario]
@IdHorario as int,
@Materia as nvarchar(100),
@IdGrado as  int,
@Descripcion as nvarchar(200),
@Inicio as Datetime,
@Fin as DateTime,
@ColorFondo as nvarchar(200),
@DiaCompleto as bit

AS
BEGIN
    Update [dbo].[Horario] set
            [Materia]=@Materia
		   ,[IdGrado]=@IdGrado
           ,[Descripcion]=@Descripcion
           ,[Inicio]=@Inicio
           ,[Fin]=@Fin
           ,[ColorFondo]=@ColorFondo
           ,[DiaCompleto]=@DiaCompleto
		where IdHorario=@IdHorario
	
END
GO
/****** Object:  StoredProcedure [dbo].[ActualizarInstitucion]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[ActualizarInstitucion]
@IdInstitucion int,
@Descripcion varchar(200),
@Telefono varchar(50),
@Encargado varchar(50),
@Direccion varchar(500),
@Provincia char(1),
@Canton char(20),
@Distrito char(2),
@Estado bit
AS
BEGIN

    Update Institucion set           
            Descripcion=@Descripcion
           ,Telefono=@Telefono
           ,Encargado=@Encargado
           ,Direccion=@Direccion
		   ,Provincia=@Provincia
		   ,Canton=@Canton
		   ,Distrito=@Distrito
		   ,Estado=@Estado
		where IdInstitucion=@IdInstitucion
	
END
GO
/****** Object:  StoredProcedure [dbo].[ActualizarMateria]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[ActualizarMateria]
@IdMateria int,
@Descripcion varchar(50),
@IdInstitucion int,
@Estado bit
AS
BEGIN

    Update Materia set           
            Descripcion=@Descripcion
           ,IdInstitucion=@IdInstitucion
		   ,Estado=@Estado
		where IdMateria=@IdMateria
	
END
GO
/****** Object:  StoredProcedure [dbo].[ActualizarMateriaGrado]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[ActualizarMateriaGrado]
@IdMateriaGrado int,
@IdMateria int,
@IdGrado int
AS
BEGIN

    Update MateriaGrado set           
           IdMateria=@IdMateria
           ,IdGrado=@IdGrado
		where IdMateriaGrado=@IdMateriaGrado
	
END
GO
/****** Object:  StoredProcedure [dbo].[ActualizarNota]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[ActualizarNota]
@IdNota int,
@IdEstudiante int,
@IdMateria int,
@Nota decimal(4,2),
@Estado bit
AS
BEGIN

    Update Nota set           
           IdEstudiante=@IdEstudiante
		   ,IdMateria=@IdMateria
		   ,Nota=@Nota
           ,Estado=@Estado
		where IdNota=@IdNota
	
END
GO
/****** Object:  StoredProcedure [dbo].[ActualizarRol]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[ActualizarRol]
@IdRol int,
@Descripcion varchar(50),
@Estado bit
AS
BEGIN

    Update Rol set           
            Descripcion=@Descripcion
		   ,Estado=@Estado
		where IdRol=@IdRol
	
END
GO
/****** Object:  StoredProcedure [dbo].[ActualizarTipoIdentificacion]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[ActualizarTipoIdentificacion]
@IdTipoIdentificacion int,
@Descripcion varchar(50),
@Estado bit
AS
BEGIN

    Update TipoIdentificacion set           
            Descripcion=@Descripcion
		   ,Estado=@Estado
		where IdTipoIdentificacion=@IdTipoIdentificacion
	
END
GO
/****** Object:  StoredProcedure [dbo].[ActualizarTipoOperacion]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[ActualizarTipoOperacion]
@IdTipoOperacion int,
@Descripcion varchar(50),
@Estado bit
AS
BEGIN

    Update TipoOperacion set           
            Descripcion=@Descripcion
		   ,Estado=@Estado
		where IdTipoOperacion=@IdTipoOperacion
	
END
GO
/****** Object:  StoredProcedure [dbo].[ActualizarUsuario]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ActualizarUsuario]
@IdUsuario int,
@IdInstitucion int,
@IdTipoIdentificacion int,
@Identificacion varchar(50),
@Nombre varchar(50),
@Apellido1 varchar(50),
@Apellido2 varchar(50),
@Password varchar(50),
@Telefono varchar(50),
@Direccion varchar(500),
@Correo varchar(100),
@FechaNacimiento datetime,
@Estado bit,
@Provincia char(1),
@Canton char(2),
@Distrito char(2)
AS
BEGIN

    Update Usuario set           
           IdInstitucion=@IdInstitucion
           ,IdTipoIdentificacion=@IdTipoIdentificacion
           ,Identificacion=@Identificacion
           ,Nombre=@Nombre
           ,Apellido1=@Apellido1
           ,Apellido2=@Apellido2
		   ,Password=@Password
		   ,Telefono=@Telefono
		   ,Direccion=@Direccion
		   ,Correo=@Correo
		   ,FechaNacimiento=@FechaNacimiento
		   ,Estado=@Estado
		   ,Provincia=@Provincia
		   ,Canton=@Canton
		   ,Distrito=@Distrito
		where IdUsuario=@IdUsuario

	
END
GO
/****** Object:  StoredProcedure [dbo].[ActualizarUsuarioRol]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[ActualizarUsuarioRol]
@Id int,
@IdUsuario int,
@IdRol int
AS
BEGIN

    Update UsuarioRol set           
           IdUsuario=@IdUsuario
           ,IdRol=@IdRol
		where Id=@Id
	
END
GO
/****** Object:  StoredProcedure [dbo].[AgregarBitacora]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[AgregarBitacora]
@Controlador varchar(50),
@Accion varchar(50),
@Error varchar(max),
@Tipo int,
@Fecha datetime,
@IdUsuario int
AS
BEGIN

Declare @Codigo int

Select @Codigo=isnull(MAX(IdBitacora),0)+1 from Bitacora

	INSERT INTO Bitacora
           (IdBitacora
           ,Controlador
           ,Accion
           ,Error
           ,Tipo
           ,Fecha
           ,IdUsuario
		   )
     VALUES
           (@Codigo
           ,@Controlador
           ,@Accion
           ,@Error
           ,@Tipo
           ,@Fecha
           ,@IdUsuario
		   )

	Select @Codigo
END
GO
/****** Object:  StoredProcedure [dbo].[AgregarCalendario]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[AgregarCalendario]
@Asunto varchar(100),
@Descripcion varchar(500),
@Inicia datetime,
@Finaliza datetime,
@TodoElDia bit,
@Color varchar(10)
AS
BEGIN

Declare @Codigo int

Select @Codigo=isnull(MAX(IdEvento),0)+1 from Calendario

	INSERT INTO Calendario
           (IdEvento
           ,Asunto
           ,Descripcion
           ,Inicia
           ,Finaliza
           ,TodoElDia
           ,Color
		   )
     VALUES
           (@Codigo
           ,@Asunto
           ,@Descripcion
           ,@Inicia
           ,@Finaliza
           ,@TodoElDia
           ,@Color
		   )

	Select @Codigo
END

GO
/****** Object:  StoredProcedure [dbo].[AgregarCalificacion]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[AgregarCalificacion]
@IdTipoOperacion int,
@IdEstudiante int,
@IdDocente int,
@IdMateria int,
@Nota decimal(4,2),
@Fecha datetime
AS
BEGIN

Declare @Codigo int

Select @Codigo=isnull(MAX(IdCalificacion),0)+1 from Calificacion

	INSERT INTO Calificacion
           (IdCalificacion
           ,IdTipoOperacion
           ,IdEstudiante
           ,IdDocente
           ,IdMateria
           ,Nota
           ,Fecha
		   )
     VALUES
           (@Codigo
           ,@IdTipoOperacion
           ,@IdEstudiante
           ,@IdDocente
           ,@IdMateria
           ,@Nota
           ,@Fecha
		   )

	Select @Codigo
END
GO
/****** Object:  StoredProcedure [dbo].[AgregarDocente]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[AgregarDocente]
@IdInstitucion int,
@IdUsuario int,
@IdMateria int,
@Estado bit
AS
BEGIN

Declare @Codigo int

Select @Codigo=isnull(MAX(IdDocente),0)+1 from Docente

	INSERT INTO Docente
           (IdDocente
           ,IdInstitucion
           ,IdUsuario     
		   ,IdMateria
		   ,Estado
		   )
     VALUES
           (@Codigo
           ,@IdInstitucion
           ,@IdUsuario
		   ,@IdMateria
		   ,@Estado
		   )

	Select @Codigo
END
GO
/****** Object:  StoredProcedure [dbo].[AgregarDocenteGrado]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[AgregarDocenteGrado]
@IdDocente int,
@IdGrado int
AS
BEGIN

Declare @Codigo int

Select @Codigo=isnull(MAX(IdDocenteGrado),0)+1 from DocenteGrado

	INSERT INTO DocenteGrado
           (IdDocenteGrado
           ,IdDocente
           ,IdGrado
		   )
     VALUES
           (@Codigo
           ,@IdDocente
           ,@IdGrado
		   )

	Select @Codigo
END
GO
/****** Object:  StoredProcedure [dbo].[AgregarEstudiante]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[AgregarEstudiante]
@IdInstitucion int,
@IdUsuario int,
@IdGrado int,
@Estado bit
AS
BEGIN

Declare @Codigo int

Select @Codigo=isnull(MAX(IdEstudiante),0)+1 from Estudiante

	INSERT INTO Estudiante
           (IdEstudiante
           ,IdInstitucion
           ,IdUsuario
		   ,IdGrado
		   ,Estado
		   )
     VALUES
           (@Codigo
           ,@IdInstitucion
           ,@IdUsuario
		   ,@IdGrado
		   ,@Estado
		   )

	Select @Codigo
END
GO
/****** Object:  StoredProcedure [dbo].[AgregarGrado]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[AgregarGrado]
@Descripcion varchar(50),
@Estado bit
AS
BEGIN

Declare @Codigo int

Select @Codigo=isnull(MAX(IdGrado),0)+1 from Grado

	INSERT INTO Grado
           (IdGrado
           ,Descripcion
           ,Estado
		   )
     VALUES
           (@Codigo
           ,@Descripcion
           ,@Estado
		   )

	Select @Codigo
END
GO
/****** Object:  StoredProcedure [dbo].[AgregarHorario]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[AgregarHorario]
@Materia as nvarchar(100),
@IdGrado as int,
@Descripcion as nvarchar(200),
@Inicio as Datetime,
@Fin as DateTime,
@ColorFondo as nvarchar(200),
@DiaCompleto as bit

AS
BEGIN
Declare @Codigo int

Select @Codigo=isnull(MAX(IdHorario),0)+1 from Horario

    INSERT INTO Horario
           ([IdHorario]
		   ,[Materia]
		   ,[IdGrado]
           ,[Descripcion]
           ,[Inicio]
           ,[Fin]
           ,[ColorFondo]
           ,[DiaCompleto])
     VALUES
           (@Codigo
           ,@Materia
		   ,@IdGrado
           ,@Descripcion
           ,@Inicio
           ,@Fin
           ,@ColorFondo
		   ,@DiaCompleto)
	
END
GO
/****** Object:  StoredProcedure [dbo].[AgregarInstitucion]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[AgregarInstitucion]
@Descripcion varchar(200),
@Telefono varchar(50),
@Encargado varchar(50),
@Direccion varchar(500),
@Provincia char(1),
@Canton char(2),
@Distrito char(2),
@Estado bit
AS
BEGIN
Declare @Codigo int

Select @Codigo=isnull(MAX(IdInstitucion),0)+1 from Institucion
	INSERT INTO Institucion
           (IdInstitucion
           ,Descripcion
		   ,Telefono
           ,Encargado
           ,Direccion
           ,Provincia
           ,Canton
           ,Distrito
		   ,Estado
		   
		   )
     VALUES
           (@Codigo
           ,@Descripcion
           ,@Telefono
           ,@Encargado 
		   ,@Direccion
           ,@Provincia
           ,@Canton
		   ,@Distrito
		   ,@Estado		   
		   )

	Select @Codigo
END

GO
/****** Object:  StoredProcedure [dbo].[AgregarMateria]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[AgregarMateria]
@Descripcion varchar(50),
@IdInstitucion int,
@Estado bit
AS
BEGIN

Declare @Codigo int

Select @Codigo=isnull(MAX(IdMateria),0)+1 from Materia

	INSERT INTO Materia
           (IdMateria
           ,Descripcion
           ,IdInstitucion
		   ,Estado
		   
		   )
     VALUES
           (@Codigo
           ,@Descripcion
           ,@IdInstitucion
		   ,@Estado
		   
		   )

	Select @Codigo
END
GO
/****** Object:  StoredProcedure [dbo].[AgregarMateriaGrado]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[AgregarMateriaGrado]
@IdMateria int,
@IdGrado int
AS
BEGIN

Declare @Codigo int

Select @Codigo=isnull(MAX(IdMateriaGrado),0)+1 from MateriaGrado

	INSERT INTO MateriaGrado
           (IdMateriaGrado
           ,IdMateria
           ,IdGrado
		   )
     VALUES
           (@Codigo
           ,@IdMateria
           ,@IdGrado
		   )

	Select @Codigo
END
GO
/****** Object:  StoredProcedure [dbo].[AgregarNota]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[AgregarNota]
@IdEstudiante int,
@IdMateria int,
@Nota decimal(4,2),
@Estado bit
AS
BEGIN

Declare @Codigo int

Select @Codigo=isnull(MAX(IdNota),0)+1 from Nota

	INSERT INTO Nota
           (IdNota
           ,IdEstudiante
           ,IdMateria
		   ,Nota
		   ,Estado		   
		   )
     VALUES
           (@Codigo
           ,@IdEstudiante
           ,@IdMateria
		   ,@Nota
		   ,@Estado		   
		   )

	Select @Codigo
END
GO
/****** Object:  StoredProcedure [dbo].[AgregarRol]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[AgregarRol]
@Descripcion varchar(50),
@Estado bit
AS
BEGIN

Declare @Codigo int

Select @Codigo=isnull(MAX(IdRol),0)+1 from Rol

	INSERT INTO Rol
           (IdRol
           ,Descripcion
		   ,Estado		   
		   )
     VALUES
           (@Codigo
           ,@Descripcion
		   ,@Estado		   
		   )

	Select @Codigo
END
GO
/****** Object:  StoredProcedure [dbo].[AgregarTipoIdentificacion]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[AgregarTipoIdentificacion]
@Descripcion varchar(50),
@Estado bit
AS
BEGIN

Declare @Codigo int

Select @Codigo=isnull(MAX(IdTipoIdentificacion),0)+1 from TipoIdentificacion

	INSERT INTO TipoIdentificacion
           (IdTipoIdentificacion
           ,Descripcion
		   ,Estado		   
		   )
     VALUES
           (@Codigo
           ,@Descripcion
		   ,@Estado		   
		   )

	Select @Codigo
END
GO
/****** Object:  StoredProcedure [dbo].[AgregarTipoOperacion]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[AgregarTipoOperacion]
@Descripcion varchar(50),
@Estado bit
AS
BEGIN

Declare @Codigo int

Select @Codigo=isnull(MAX(IdTipoOperacion),0)+1 from TipoOperacion

	INSERT INTO TipoOperacion
           (IdTipoOperacion
           ,Descripcion
		   ,Estado		   
		   )
     VALUES
           (@Codigo
           ,@Descripcion
		   ,@Estado		   
		   )

	Select @Codigo
END
GO
/****** Object:  StoredProcedure [dbo].[AgregarUsuario]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[AgregarUsuario]
@IdInstitucion int,
@IdTipoIdentificacion int,
@Identificacion varchar(50),
@Nombre varchar(50),
@Apellido1 varchar(50),
@Apellido2 varchar(50),
@Password varchar(50),
@Telefono varchar(50),
@Direccion varchar(500),
@Correo varchar(100),
@FechaNacimiento datetime,
@Estado bit,
@Provincia char(1),
@Canton char(2),
@Distrito char(2)
AS
BEGIN

Declare @Codigo int

Select @Codigo=isnull(MAX(IdUsuario),0)+1 from Usuario


	INSERT INTO Usuario
           (IdUsuario
           ,IdInstitucion
           ,IdTipoIdentificacion
           ,Identificacion
           ,Nombre
           ,Apellido1
           ,Apellido2
		   ,Password
		   ,Telefono
		   ,Direccion
		   ,Correo
		   ,FechaNacimiento
		   ,Estado
		   ,Provincia
		   ,Canton
		   ,Distrito
		   )
     VALUES
           (@Codigo
           ,@IdInstitucion
           ,@IdTipoIdentificacion
           ,@Identificacion
           ,@Nombre
           ,@Apellido1
           ,@Apellido2
		   ,@Password
		   ,@Telefono
		   ,@Direccion
		   ,@Correo
		   ,@FechaNacimiento
		   ,@Estado
		   ,@Provincia
		   ,@Canton
		   ,@Distrito
		   )

	Select @Codigo
END
GO
/****** Object:  StoredProcedure [dbo].[AgregarUsuarioRol]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[AgregarUsuarioRol]
@IdUsuario int,
@IdRol int
AS
BEGIN

Declare @Codigo int

Select @Codigo=isnull(MAX(Id),0)+1 from UsuarioRol

	INSERT INTO UsuarioRol
           (Id
           ,IdUsuario
           ,IdRol
		   )
     VALUES
           (@Codigo
           ,@IdUsuario
           ,@IdRol
		   )

	Select @Codigo
END
GO
/****** Object:  StoredProcedure [dbo].[Cantones]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[Cantones]
@Provincia char(1)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * from Canton where Provincia=@Provincia order by Provincia,Canton
END
GO
/****** Object:  StoredProcedure [dbo].[ConsultaBitacora]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[ConsultaBitacora]
@IdBitacora as int

AS
BEGIN
   Select * from Bitacora where IdBitacora=@IdBitacora
	
END
GO
/****** Object:  StoredProcedure [dbo].[ConsultaCalendario]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[ConsultaCalendario]
@IdEvento int
AS
BEGIN
   Select * from Calendario where IdEvento = @IdEvento
	
END
GO
/****** Object:  StoredProcedure [dbo].[ConsultaCalificacion]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[ConsultaCalificacion]
@IdCalificacion as int

AS
BEGIN
   Select * from Calificacion where IdCalificacion=@IdCalificacion
	
END
GO
/****** Object:  StoredProcedure [dbo].[ConsultaDocente]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[ConsultaDocente]
@IdDocente as int

AS
BEGIN
   Select * from Docente where IdDocente=@IdDocente
	
END
GO
/****** Object:  StoredProcedure [dbo].[ConsultaDocenteGrado]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[ConsultaDocenteGrado]
@IdDocenteGrado as int

AS
BEGIN
   Select * from DocenteGrado where IdDocenteGrado=@IdDocenteGrado
	
END
GO
/****** Object:  StoredProcedure [dbo].[ConsultaEstudiante]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[ConsultaEstudiante]
@IdEstudiante as int

AS
BEGIN
   Select * from Estudiante where IdEstudiante=@IdEstudiante
	
END
GO
/****** Object:  StoredProcedure [dbo].[ConsultaGrado]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[ConsultaGrado]
@IdGrado as int

AS
BEGIN
   Select * from Grado where IdGrado=@IdGrado
	
END
GO
/****** Object:  StoredProcedure [dbo].[ConsultaGradoPorEstudiante]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ConsultaGradoPorEstudiante]
    @IdUsuario int
AS
BEGIN
    SET NOCOUNT ON;
    select IdGrado from Estudiante e
    where e.IdUsuario=@IdUsuario
   
END
GO
/****** Object:  StoredProcedure [dbo].[ConsultaHorario]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[ConsultaHorario]
@IdHorario as int

AS
BEGIN
   Select * from Horario where IdHorario=@IdHorario
	
END
GO
/****** Object:  StoredProcedure [dbo].[ConsultaInstitucion]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[ConsultaInstitucion]
@IdInstitucion as int

AS
BEGIN
   Select * from Institucion where IdInstitucion=@IdInstitucion
	
END
GO
/****** Object:  StoredProcedure [dbo].[ConsultaMateria]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[ConsultaMateria]
@IdMateria as int

AS
BEGIN
   Select * from Materia where IdMateria=@IdMateria
	
END
GO
/****** Object:  StoredProcedure [dbo].[ConsultaMateriaGrado]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[ConsultaMateriaGrado]
@IdMateriaGrado as int

AS
BEGIN
   Select * from MateriaGrado where IdMateriaGrado=@IdMateriaGrado
	
END
GO
/****** Object:  StoredProcedure [dbo].[ConsultaMateriasPorEstudiante]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[ConsultaMateriasPorEstudiante]
    @IdUsuario int
AS
BEGIN
    SET NOCOUNT ON;
    select m.Descripcion from Estudiante e
    inner join Grado g on e.IdGrado=g.IdGrado
	inner join MateriaGrado mg on mg.IdGrado=g.IdGrado
	inner join Materia m on m.IdMateria=mg.IdMateria

    where e.IdUsuario=@IdUsuario
   
END
GO
/****** Object:  StoredProcedure [dbo].[ConsultaNota]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create PROCEDURE [dbo].[ConsultaNota]
@IdNota as int

AS
BEGIN
   Select * from Nota where IdNota=@IdNota
	
END
GO
/****** Object:  StoredProcedure [dbo].[ConsultaNotaLista]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[ConsultaNotaLista]
@id int
AS
BEGIN
    select n.IdNota, u.Identificacion,  u.Nombre+' '+u.Apellido1+' '+u.Apellido2 'Nombre', m.Descripcion 'Materia', n.Nota, n.Estado
from Nota n 
inner join Estudiante e on e.IdEstudiante = n.IdEstudiante
inner join Materia m on m.IdMateria = n.IdMateria
inner join Usuario u on u.IdUsuario = e.IdUsuario where u.IdUsuario=@id

END
GO
/****** Object:  StoredProcedure [dbo].[ConsultarBitacora]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[ConsultarBitacora]
AS
BEGIN
   Select * from Bitacora
	
END
GO
/****** Object:  StoredProcedure [dbo].[ConsultarCalendario]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[ConsultarCalendario]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * From Calendario
END
GO
/****** Object:  StoredProcedure [dbo].[ConsultarCalificacion]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[ConsultarCalificacion]
AS
BEGIN
   Select * from Calificacion
	
END
GO
/****** Object:  StoredProcedure [dbo].[ConsultarCalificacionLista]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[ConsultarCalificacionLista]
AS
BEGIN
     select c.IdCalificacion, t.Descripcion 'TipoOperacion', u.Nombre+' '+u.Apellido1+' '+u.Apellido2 'NombreEstudiante',
	 ud.Nombre+' '+ud.Apellido1+' '+ud.Apellido2 'NombreDocente', m.Descripcion 'Materia', c.Nota, c.Fecha
from Calificacion c 
inner join TipoOperacion t on t.IdTipoOperacion = c.IdTipoOperacion
inner join Estudiante e on e.IdEstudiante = c.IdEstudiante
inner join Docente d on d.IdDocente = c.IdDocente
inner join Materia m on m.IdMateria = c.IdMateria
inner join Usuario u on u.IdUsuario = e.IdUsuario
inner join Usuario ud on ud.IdUsuario = d.IdUsuario;

END
GO
/****** Object:  StoredProcedure [dbo].[ConsultarDocente]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[ConsultarDocente]
AS
BEGIN
   Select * from Docente
	
END
GO
/****** Object:  StoredProcedure [dbo].[ConsultarDocenteGrado]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create PROCEDURE [dbo].[ConsultarDocenteGrado]
AS
BEGIN
   Select * from DocenteGrado
	
END
GO
/****** Object:  StoredProcedure [dbo].[ConsultarDocenteGradoLista]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[ConsultarDocenteGradoLista]
AS
BEGIN
    select dg.IdDocenteGrado, u.Nombre+' '+u.Apellido1+' '+u.Apellido2  'NombreDocente', g.Descripcion 'Grado'

from DocenteGrado dg 
inner join Docente d on dg.IdDocente = d.IdDocente
inner join Grado g on g.IdGrado = dg.IdGrado
inner join Usuario u on u.IdUsuario = d.IdUsuario;

END
GO
/****** Object:  StoredProcedure [dbo].[ConsultarDocenteGradoVB]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create PROCEDURE [dbo].[ConsultarDocenteGradoVB]
AS
BEGIN
   Select d.IdDocente,  u.Nombre+' '+u.Apellido1+' '+u.Apellido2  'Nombre' from Docente d
	inner join Usuario u on u.IdUsuario = d.IdUsuario
END
GO
/****** Object:  StoredProcedure [dbo].[ConsultarDocenteLista]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[ConsultarDocenteLista]
AS
BEGIN
  select idDocente, i.Descripcion 'InstitucionNombre', u.Nombre+' '+u.Apellido1+' '+u.Apellido2 'Nombre',
m.Descripcion 'MateriaNombre', d.Estado
from Docente d 
inner join Usuario u on d.IdUsuario = u.IdUsuario
inner join Institucion i on i.IdInstitucion = d.IdInstitucion
inner join Materia m on m.IdMateria = d.IdMateria;

END
GO
/****** Object:  StoredProcedure [dbo].[ConsultarEstudiante]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[ConsultarEstudiante]
AS
BEGIN
   Select * from Estudiante
	
END
GO
/****** Object:  StoredProcedure [dbo].[ConsultarEstudianteLista]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[ConsultarEstudianteLista]
AS
BEGIN
  select e.IdEstudiante, i.Descripcion 'Institucion',u.Identificacion 'Cedula', u.Nombre+' '+u.Apellido1+' '+u.Apellido2 'Nombre',
g.Descripcion 'Grado', e.Estado
from Estudiante e 
inner join Usuario u on e.IdUsuario = u.IdUsuario
inner join Institucion i on i.IdInstitucion = e.IdInstitucion
inner join Grado g on g.IdGrado = e.IdGrado;

END
GO
/****** Object:  StoredProcedure [dbo].[ConsultarGrado]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create PROCEDURE [dbo].[ConsultarGrado]
AS
BEGIN
   Select * from Grado
	
END
GO
/****** Object:  StoredProcedure [dbo].[ConsultarHorario]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [dbo].[ConsultarHorario]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * From Horario
END
GO
/****** Object:  StoredProcedure [dbo].[ConsultarInstitucion]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[ConsultarInstitucion]
AS
BEGIN
   Select * from Institucion
	
END
GO
/****** Object:  StoredProcedure [dbo].[ConsultarMateria]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[ConsultarMateria]
AS
BEGIN
   Select * from Materia
	
END
GO
/****** Object:  StoredProcedure [dbo].[ConsultarMateriaGrado]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create PROCEDURE [dbo].[ConsultarMateriaGrado]
AS
BEGIN
   Select * from MateriaGrado
	
END
GO
/****** Object:  StoredProcedure [dbo].[ConsultarMateriaLista]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create PROCEDURE [dbo].[ConsultarMateriaLista]
AS
BEGIN
    select m.IdMateria, m.Descripcion , i.Descripcion 'Institucion', m.Estado
from Materia m 
inner join Institucion i on i.IdInstitucion = m.IdInstitucion

END
GO
/****** Object:  StoredProcedure [dbo].[ConsultarNota]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



create PROCEDURE [dbo].[ConsultarNota]
AS
BEGIN
   Select * from Nota
	
END
GO
/****** Object:  StoredProcedure [dbo].[ConsultarNotaLista]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create PROCEDURE [dbo].[ConsultarNotaLista]
AS
BEGIN
    select n.IdNota,  u.Nombre+' '+u.Apellido1+' '+u.Apellido2 'Nombre', m.Descripcion 'Materia', n.Nota, n.Estado
from Nota n 
inner join Estudiante e on e.IdEstudiante = n.IdEstudiante
inner join Materia m on m.IdMateria = n.IdMateria
inner join Usuario u on u.IdUsuario = e.IdUsuario
END
GO
/****** Object:  StoredProcedure [dbo].[ConsultaRol]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[ConsultaRol]
@IdRol as int

AS
BEGIN
   Select * from Rol where IdRol=@IdRol
	
END
GO
/****** Object:  StoredProcedure [dbo].[ConsultaRolPorUsuario]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ConsultaRolPorUsuario]
    @correo varchar(20)
AS
BEGIN
    SET NOCOUNT ON;
    select r.Descripcion from UsuarioRol ur
    inner join Usuario u on ur.IdUsuario=u.IdUsuario
    inner join Rol r on ur.IdRol = r.IdRol
    where u.Correo=@correo
   
END
GO
/****** Object:  StoredProcedure [dbo].[ConsultarRol]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[ConsultarRol]
AS
BEGIN
   Select * from Rol
	
END
GO
/****** Object:  StoredProcedure [dbo].[ConsultarTipoIdentificacion]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[ConsultarTipoIdentificacion]
AS
BEGIN
   Select * from TipoIdentificacion
	
END
GO
/****** Object:  StoredProcedure [dbo].[ConsultarTipoOperacion]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[ConsultarTipoOperacion]
AS
BEGIN
   Select * from TipoOperacion
	
END
GO
/****** Object:  StoredProcedure [dbo].[ConsultarUsuario]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create PROCEDURE [dbo].[ConsultarUsuario]
AS
BEGIN
   Select * from Usuario
	
END
GO
/****** Object:  StoredProcedure [dbo].[ConsultarUsuarioLista]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[ConsultarUsuarioLista]
AS
BEGIN
  select u.IdUsuario, i.Descripcion 'Institucion', t.Descripcion 'TipoIdentificacion',u.Identificacion, u.Nombre+' '+u.Apellido1+' '+u.Apellido2 'Nombre',
   u.Correo, u.Estado
from Usuario u  
inner join TipoIdentificacion t on t.IdTipoIdentificacion = u.IdTipoIdentificacion
inner join Institucion i on i.IdInstitucion = u.IdInstitucion

END
GO
/****** Object:  StoredProcedure [dbo].[ConsultarUsuarioRol]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create PROCEDURE [dbo].[ConsultarUsuarioRol]
AS
BEGIN
   Select * from UsuarioRol
	
END
GO
/****** Object:  StoredProcedure [dbo].[ConsultarUsuarioRolLista]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create PROCEDURE [dbo].[ConsultarUsuarioRolLista]
AS
BEGIN
  select ur.Id,  u.Nombre+' '+u.Apellido1+' '+u.Apellido2 'Nombre', r.Descripcion 'Rol' 
from UsuarioRol ur 
inner join Usuario u on ur.IdUsuario = u.IdUsuario
inner join Rol r on ur.IdRol = r.IdRol


END
GO
/****** Object:  StoredProcedure [dbo].[ConsultarUsuarioViewBag]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create PROCEDURE [dbo].[ConsultarUsuarioViewBag]
AS
BEGIN
   Select IdUsuario,  Nombre+' '+Apellido1+' '+Apellido2 'Nombre' from Usuario
	
END
GO
/****** Object:  StoredProcedure [dbo].[ConsultaTipoIdentificacion]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[ConsultaTipoIdentificacion]
@IdTipoIdentificacion as int

AS
BEGIN
   Select * from TipoIdentificacion where IdTipoIdentificacion=@IdTipoIdentificacion
	
END
GO
/****** Object:  StoredProcedure [dbo].[ConsultaTipoOperacion]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[ConsultaTipoOperacion]
@IdTipoOperacion as int

AS
BEGIN
   Select * from TipoOperacion where IdTipoOperacion=@IdTipoOperacion
	
END
GO
/****** Object:  StoredProcedure [dbo].[ConsultaUR]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create PROCEDURE [dbo].[ConsultaUR]
	@correo varchar(20),
	@descripcionRol varchar(20)
AS
BEGIN
	SET NOCOUNT ON;
	declare @resultado bit =0;

	if exists(
	select * from UsuarioRol ur
	inner join Usuario u on ur.IdUsuario=u.IdUsuario
	inner join Rol r on ur.IdRol = r.IdRol
	where r.Descripcion=@descripcionRol and u.Correo=@correo)
	set @resultado = 1


	 
	 select @resultado
END
GO
/****** Object:  StoredProcedure [dbo].[ConsultaUsuario]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[ConsultaUsuario]
@IdUsuario as int
AS
BEGIN
   Select * from Usuario where IdUsuario = @IdUsuario;
END
GO
/****** Object:  StoredProcedure [dbo].[ConsultaUsuarioPorRol]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create PROCEDURE [dbo].[ConsultaUsuarioPorRol]
	
	@descripcionRol varchar(20)
AS
BEGIN
	SET NOCOUNT ON;
	select u.Correo from UsuarioRol ur
	inner join Usuario u on ur.IdUsuario=u.IdUsuario
	inner join Rol r on ur.IdRol = r.IdRol
	where r.Descripcion=@descripcionRol 
END
GO
/****** Object:  StoredProcedure [dbo].[ConsultaUsuarioRol]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create PROCEDURE [dbo].[ConsultaUsuarioRol]
@Id as int

AS
BEGIN
   Select * from UsuarioRol where Id=@Id
	
END
GO
/****** Object:  StoredProcedure [dbo].[Distritos]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Distritos]
@Provincia char(1),
@Canton char(2)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * from Distrito where Provincia=@Provincia and Canton=@Canton
	order by Provincia,Canton,Distrito
END
GO
/****** Object:  StoredProcedure [dbo].[EliminarCalendario]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[EliminarCalendario]
@IdEvento as int

AS
BEGIN
   Delete Calendario
		where IdEvento=@IdEvento
	
END
GO
/****** Object:  StoredProcedure [dbo].[EliminarCalificacion]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[EliminarCalificacion]
@IdCalificacion as int

AS
BEGIN
   Delete Calificacion 
		where IdCalificacion=@IdCalificacion
	
END
GO
/****** Object:  StoredProcedure [dbo].[EliminarDocente]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[EliminarDocente]
@IdDocente as int

AS
BEGIN
   Delete Docente 
		where IdDocente=@IdDocente
	
END
GO
/****** Object:  StoredProcedure [dbo].[EliminarDocenteGrado]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[EliminarDocenteGrado]
@IdDocenteGrado int

AS
BEGIN
   Delete DocenteGrado
		where IdDocenteGrado=@IdDocenteGrado
	
END
GO
/****** Object:  StoredProcedure [dbo].[EliminarEstudiante]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[EliminarEstudiante]
@IdEstudiante as int

AS
BEGIN
   Delete Estudiante 
		where IdEstudiante=@IdEstudiante
	
END
GO
/****** Object:  StoredProcedure [dbo].[EliminarGrado]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[EliminarGrado]
@IdGrado int

AS
BEGIN
   Delete Grado
		where IdGrado=@IdGrado
	
END
GO
/****** Object:  StoredProcedure [dbo].[EliminarHorario]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
create PROCEDURE [dbo].[EliminarHorario]
@IdHorario as int

AS
BEGIN
   Delete [dbo].[Horario] 
		where IdHorario=@IdHorario
	
END
GO
/****** Object:  StoredProcedure [dbo].[EliminarInstitucion]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[EliminarInstitucion]
@IdInstitucion as int

AS
BEGIN
   Delete Institucion 
		where IdInstitucion=@IdInstitucion
	
END
GO
/****** Object:  StoredProcedure [dbo].[EliminarMateria]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[EliminarMateria]
@IdMateria as int

AS
BEGIN
   Delete Materia 
		where IdMateria=@IdMateria
	
END
GO
/****** Object:  StoredProcedure [dbo].[EliminarMateriaGrado]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[EliminarMateriaGrado]
@IdMateriaGrado int

AS
BEGIN
   Delete MateriaGrado
		where IdMateriaGrado=@IdMateriaGrado
	
END
GO
/****** Object:  StoredProcedure [dbo].[EliminarNota]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



create PROCEDURE [dbo].[EliminarNota]
@IdNota as int

AS
BEGIN
   Delete Nota 
		where IdNota=@IdNota
	
END

GO
/****** Object:  StoredProcedure [dbo].[EliminarRol]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[EliminarRol]
@IdRol as int

AS
BEGIN
   Delete Rol 
		where IdRol=@IdRol
	
END
GO
/****** Object:  StoredProcedure [dbo].[EliminarTipoIdentificacion]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[EliminarTipoIdentificacion]
@IdTipoIdentificacion as int

AS
BEGIN
   Delete TipoIdentificacion 
		where IdTipoIdentificacion=@IdTipoIdentificacion
	
END
GO
/****** Object:  StoredProcedure [dbo].[EliminarTipoOperacion]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[EliminarTipoOperacion]
@IdTipoOperacion as int

AS
BEGIN
   Delete TipoOperacion 
		where IdTipoOperacion=@IdTipoOperacion
	
END
GO
/****** Object:  StoredProcedure [dbo].[EliminarUsuario]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[EliminarUsuario]
@IdUsuario as int

AS
BEGIN
   Delete Usuario
		where IdUsuario=@IdUsuario
	
END
GO
/****** Object:  StoredProcedure [dbo].[EliminarUsuarioRol]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[EliminarUsuarioRol]
@Id as int

AS
BEGIN
   Delete UsuarioRol
		where Id=@Id
	
END
GO
/****** Object:  StoredProcedure [dbo].[ExisteUsuario]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[ExisteUsuario]
@Correo as varchar(100),
@Password as varchar(50)
AS
BEGIN

    -- Insert statements for procedure here
	SELECT * from Usuario where Correo=@Correo and Password=@Password
END

GO
/****** Object:  StoredProcedure [dbo].[Provincias]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Provincias]
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT * from Provincia order by Provincia
END
GO
/****** Object:  StoredProcedure [dbo].[UsuarioRoles]    Script Date: 24/8/2020 17:30:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[UsuarioRoles]
@IdUsuario as int
AS
BEGIN
    Select R.IdRol,R.Descripcion as Rol from UsuarioRol UR 
    inner Join Rol R on (R.IdRol=UR.IdRol)
    where UR.IdUsuario=@IdUsuario

 

END
GO
