﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class clsInstitucion
    {
		public List<ConsultarInstitucionResult> ConsultarInstitucion()
		{
			try
			{
				DatosDataContext db = new DatosDataContext();
				List<ConsultarInstitucionResult> datos = db.ConsultarInstitucion().ToList();
				return datos;
			}
			catch (Exception)
			{

				throw;
			}

		}
		public ConsultaInstitucionResult ConsultaInstitucion(int IdInstitucion)
		{
			try
			{
				DatosDataContext db = new DatosDataContext();
				ConsultaInstitucionResult dato = db.ConsultaInstitucion(IdInstitucion).FirstOrDefault();
				return dato;
			}
			catch (Exception)
			{

				throw;
			}
		}
		public bool ActualizarInstitucion(int IdInstitucion, string Descripcion, string Telefono, string Encargado,
			string Direccion, char Provincia, string Canton, string Distrito, bool Estado)
		{
			try
			{
				DatosDataContext db = new DatosDataContext();
				db.ActualizarInstitucion(IdInstitucion, Descripcion, Telefono, Encargado, Direccion, Provincia,
					Canton, Distrito, Estado);
				return true;
			}
			catch (Exception)
			{
				return false;
				throw;
			}
		}
		public bool AgregarInstitucion(string Descripcion, string Telefono, string Encargado,
			string Direccion, char Provincia, string Canton, string Distrito, bool Estado)
		{
			try
			{
				DatosDataContext db = new DatosDataContext();
				db.AgregarInstitucion(Descripcion, Telefono, Encargado, Direccion, Provincia,
					Canton, Distrito, Estado);
				return true;
			}
			catch (Exception)
			{
				return false;
				throw;
			}
		}
		public bool EliminarInstitucion(int IdInstitucion)
		{
			try
			{
				DatosDataContext db = new DatosDataContext();
				db.EliminarInstitucion(IdInstitucion);
				return true;
			}
			catch (Exception)
			{
				return false;
				throw;
			}
		}
	}
}
