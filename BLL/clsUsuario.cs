﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace BLL
{
    public class clsUsuario
    {
		public List<ConsultarUsuarioResult> ConsultarUsuario()
		{
			try
			{
				DatosDataContext db = new DatosDataContext();
				List<ConsultarUsuarioResult> datos = db.ConsultarUsuario().ToList();
				return datos;
			}
			catch (Exception ex)
			{

				throw;
			}

		}
		public List<ConsultarUsuarioViewBagResult> ConsultarUsuarioViewBag()
		{
			try
			{
				DatosDataContext db = new DatosDataContext();
				List<ConsultarUsuarioViewBagResult> datos = db.ConsultarUsuarioViewBag().ToList();
				return datos;
			}
			catch (Exception ex)
			{

				throw;
			}

		}
		public List<ConsultarUsuarioListaResult> ConsultarUsuarioLista()
		{
			try
			{
				DatosDataContext db = new DatosDataContext();
				List<ConsultarUsuarioListaResult> datos = db.ConsultarUsuarioLista().ToList();
				return datos;
			}
			catch (Exception ex)
			{

				throw;
			}

		}
		public ConsultaUsuarioResult ConsultaUsuario(int IdUsuario)
		{
			try
			{
				DatosDataContext db = new DatosDataContext();
				ConsultaUsuarioResult dato = db.ConsultaUsuario(IdUsuario).FirstOrDefault();
				return dato;
			}
			catch (Exception ex)
			{

				throw;
			}
		}
		public bool ActualizarUsuario(int IdUsuario, int IdInstitucion, int IdTipoIdentificacion,
			string Identificacion,
			string Nombre, string Apellido1, string Apellido2, string Password, string Telefono,
			string Direccion, string Correo, DateTime FechaNacimiento, bool Estado, char Provincia, string Canton, string Distrito)
		{
			try
			{
				DatosDataContext db = new DatosDataContext();
				db.ActualizarUsuario(IdUsuario, IdInstitucion, IdTipoIdentificacion,
					Identificacion, Nombre, Apellido1,
					Apellido2, Password, Telefono, Direccion, Correo, FechaNacimiento, Estado, Provincia, Canton, Distrito);
				return true;
			}
			catch (Exception ex)
			{
				return false;
				throw;
			}
		}
		public bool AgregarUsuario(int IdInstitucion, int IdTipoIdentificacion,
			string Identificacion,
			string Nombre, string Apellido1, string Apellido2, string Password, string Telefono,
			string Direccion, string Correo, DateTime FechaNacimiento, bool Estado, char Provincia, string Canton, string Distrito)
		{
			try
			{
				DatosDataContext db = new DatosDataContext();
				db.AgregarUsuario(IdInstitucion, IdTipoIdentificacion,
					Identificacion, Nombre, Apellido1,
					Apellido2, Password, Telefono, Direccion, Correo, FechaNacimiento, Estado, Provincia, Canton, Distrito);
				return true;
			}
			catch (Exception ex)
			{
				return false;
				throw;
			}
		}
		public bool EliminarUsuario(int IdUsuario)
		{
			try
			{
				DatosDataContext db = new DatosDataContext();
				db.EliminarUsuario(IdUsuario);
				return true;
			}
			catch (Exception ex)
			{
				return false;
				throw;
			}
		}
        public ExisteUsuarioResult ValidaUsuario(string Correo, string Password)
        {
            try
            {
                DatosDataContext dc = new DatosDataContext();
                ExisteUsuarioResult data = dc.ExisteUsuario(Correo, Password).FirstOrDefault();
                return data;
            }
            catch (Exception ex)
            {
                throw;
            }

        }
		public List<UsuarioRolesResult> ConsultarRolesUsuario(int IdUsuario)
		{
			try
			{
				DatosDataContext db = new DatosDataContext();
				List<UsuarioRolesResult> datos = db.UsuarioRoles(IdUsuario).ToList();
				return datos;
			}
			catch (Exception ex)
			{

				throw;
			}
		}
	}
}
