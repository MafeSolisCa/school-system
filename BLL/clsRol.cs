﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class clsRol
	{
		public List<ConsultarRolResult> ConsultarRol()
		{
			try
			{
				DatosDataContext db = new DatosDataContext();
				List<ConsultarRolResult> datos = db.ConsultarRol().ToList();
				return datos;
			}
			catch (Exception ex)
			{

				throw;
			}

		}

		public List<ConsultaUsuarioPorRolResult> ConsultaUsuarioPorRol(string descripcionRol)
		{
			try
			{
				DatosDataContext db = new DatosDataContext();
				return db.ConsultaUsuarioPorRol(descripcionRol).ToList();
			}
			catch (Exception)
			{

				throw;
			}
		}

		public List<ConsultaRolPorUsuarioResult> ConsultaRolPorUsuario(string correo)
		{
			try
			{
				DatosDataContext db = new DatosDataContext();
				return db.ConsultaRolPorUsuario(correo).ToList();
			}
			catch (Exception)
			{

				throw;
			}
		}



		public bool ConsultaUR(string correo, string descripcionRol)
        {
			try
			{
				DatosDataContext db = new DatosDataContext();
                return db.ConsultaUR(correo, descripcionRol).FirstOrDefault().Column1 == true;
            }
            catch (Exception)
			{
				throw;
			}
		}



		public ConsultaRolResult ConsultaRol(int IdRol)
		{
			try
			{
				DatosDataContext db = new DatosDataContext();
				ConsultaRolResult dato = db.ConsultaRol(IdRol).FirstOrDefault();
				return dato;
			}
			catch (Exception ex)
			{

				throw;
			}
		}



		public bool ActualizarRol(int IdRol, string Descripcion, bool Estado)
		{
			try
			{
				DatosDataContext db = new DatosDataContext();
				db.ActualizarRol(IdRol, Descripcion, Estado);
				return true;
			}
			catch (Exception ex)
			{
				return false;
				throw;
			}
		}
		public bool AgregarRol(string Descripcion, bool Estado)
		{
			try
			{
				DatosDataContext db = new DatosDataContext();
				db.AgregarRol(Descripcion, Estado);
				return true;
			}
			catch (Exception ex)
			{
				return false;
				throw;
			}
		}
		public bool EliminarRol(int IdRol)
		{
			try
			{
				DatosDataContext db = new DatosDataContext();
				db.EliminarRol(IdRol);
				return true;
			}
			catch (Exception ex)
			{
				return false;
				throw;
			}
		}
	}
}
